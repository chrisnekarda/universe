// esize_rcpt5b.do
//
// Calculate firm emplsize associated with the $5b receipts cut-off
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

// variable for date-time stamp
local d = string(clock("`c(current_date)' `c(current_time)'", "DMY hms"), "%tcCCYYNNDD!THHMMSS")

cap log close
log using ../../log/esize_rcpt5b_`d'.log, text replace

noi disp as text "$S_DATE $S_TIME  begin esize_rcpt5b.do"
est drop _all

tempfile rcptsize


// start from the 2017 SUSB rcptsize file with [2.5 B, 5 B) and [5 B, ∞) bins
use ../../data/processed/susb_rcptsize_2017_rcpt5b.dta, clear
drop naics2

// compute rcptsize CDF for employment (excluding total category)
foreach xx in empl {
   bys sect (rsize): gen cum = sum(`xx') if rsize != 0
   egen tot = max(cum), by(sect)
   gen rcptcdf_`xx' = cum / tot
   drop cum tot
}

// keep only the $5B point
keep if rsize == 5e9 - 1

rename rcptcdf_empl rcptcdf_empl_rcpt5b


// keep only empl CDF
keep sect rcptcdf_empl_rcpt5b

save `rcptsize'


// start from the 2017 SUSB emplsize file with [10k, 15k) and [15k, 20k) bins
use ../../data/processed/susb_emplsize_2017_empl15k.dta, clear
drop naics2

// compute empirical CDFs (excluding total category)
foreach xx in firm estb empl payr rcpt {
   bys sect (esize): gen cum = sum(`xx') if esize != 0
   egen tot = max(cum), by(sect)
   gen emplcdf_`xx' = cum / tot
   drop cum tot
}

// merge in fitted CDFs by firm empl size
merge 1:m sect ebin using ../../data/interim/interp_empl15k.dta, update keepusing(ilesize ebin esize lesize emplcdf_*_hat) nogen
sort sect esize


// merge in rcptsize $5B CDF location
merge m:1 sect using `rcptsize', nogen

order sect ilesize ebin esize lesize
sort sect ilesize


// find the employment size for firm with $5b in receipts
gen absdif5b = abs(emplcdf_empl_hat - rcptcdf_empl_rcpt5b)
egen mindif5b = min(absdif5b), by(sect)

// this is closest the emplsize and rcptsize empl CDFs get to each other
gen foo = ilesize if absdif5b == mindif5b
egen ilesize_rcpt5b = max(foo), by(sect)
drop foo


// rename the variables
rename esize esize_rcpt5b
foreach xx in firm empl estb payr rcpt {
   rename emplcdf_`xx'_hat emplcdf_`xx'_rcpt5b
}

// save interim results
compress
save ../../data/interim/esize_rcpt5b.dta, replace


// this is closest the emplsize and rcptsize empl CDFs get to each other
keep if ilesize == ilesize_rcpt5b

// these sectors do not have a max firm size > $5b
drop if rcptcdf_empl_rcpt5b == .

// these are all we need
keep sect esize_rcpt5b emplcdf_*_rcpt5b

compress
save ../../data/processed/esize_rcpt5b.dta, replace

noi list, clean noobs ab(20)


noi disp as text "$S_DATE $S_TIME  end esize_rcpt5b.do"
cap log close

if "$S_CONSOLE" == "console" & "$batch" != "1" exit, STATA clear
