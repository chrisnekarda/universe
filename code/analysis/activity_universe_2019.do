// activity_universe_2019.do
//
// Collapse to table values
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin activity_universe_2019.do"

tempfile railroads farms nonemp


// RAILROADS //

// load RR activity data and prepare for merge

use ../../data/processed/activity_railroads_2019.dta, clear

// put these into NAICS 48-49
gen byte sect = 9
drop naics

// all are for-profit
gen byte lfo3 = 1

// rename for merge
foreach xx in firm estb empl payr rcpt {
   rename `xx' `xx'_rr
}

compress
save `railroads'



// FARMS //

use ../../data/processed/activity_farms_2019.dta, clear

// Define small farms as less than 1m revenue,
// the SBA definition for almost every 111/112 industry

gen byte size = 1 if rbin <= 2  // <$1m in revenue
replace size = 2 if rbin >= 3  // >=$1m in revenue

collapse (sum) firm estb empl payr rcpt, by(sect size)

// create new LFO for farms
gen byte lfo3 = 4

order sect lfo3 size firm estb empl payr rcpt

compress
save `farms'



// NONEMPLOYERS //

use ../../data/processed/activity_nonemp_2019.dta, clear

// these are small by definition
gen size = 1

collapse (sum) firm estb empl payr rcpt, by(sect size)

// create a category for nonemployers
gen lfo3 = 5

order sect lfo3 size firm estb empl payr rcpt

compress
save `nonemp'



// BRING IT ALL TOGETHER //

// start from SUSB data
use ../../data/processed/activity_susb_lfo_2019.dta, clear

// drop the total category, since we'll sum up from the details
drop if sect == 1

collapse (sum) firm estb empl payr rcpt, by(lfo3 sect size)

// rename for merge
foreach xx in firm estb empl payr rcpt {
   rename `xx' `xx'_susb
}

// merge in railroads data
merge 1:1 sect size lfo3 using `railroads', nogen

// compute totals
foreach xx in firm estb empl payr rcpt {
   egen `xx' = rowtotal(`xx'_*)
}
drop *_susb *_rr

// merge in farms and nonemployers
merge 1:1 sect lfo3 size using `farms', nogen
merge 1:1 sect lfo3 size using `nonemp', nogen

label def l_lfo3 4 "Farms", add
label def l_lfo3 5 "Nonemployers", add

label def l_size 0 "Total", add

rename lfo3 org_type

sort sect org_type size

// define entity "class"
gen byte class = .
replace class = 1 if org_type == 1 & size == 1
replace class = 2 if org_type == 1 & size == 2
replace class = 3 if org_type == 1 & size == 3
replace class = 4 if org_type == 2 & size == 1
replace class = 5 if org_type == 2 & size == 2
replace class = 6 if org_type == 2 & size == 3
replace class = 7 if org_type == 3
replace class = 8 if org_type == 4 & size == 1
replace class = 9 if org_type == 4 & size == 2
replace class = 10 if org_type == 5

label var class "Entity class"

label def l_class  0 "Total", replace
label def l_class  1 "For-profit, small", add
label def l_class  2 "For-profit, medium", add
label def l_class  3 "For-profit, large", add
label def l_class  4 "Nonprofit, small", add
label def l_class  5 "Nonprofit, medium", add
label def l_class  6 "Nonprofit, large", add
label def l_class  7 "Government-owned business", add
label def l_class  8 "Farm, small", add
label def l_class  9 "Farm, medium", add
label def l_class 10 "Nonemployer", add

label val class l_class

format firm estb empl payr rcpt %16.0fc

// label data
label var firm "Number of firms"
label var estb "Number of establishments"
label var empl "Employment (with noise)"
label var payr "Annual payroll ($1,000, with noise)"
label var rcpt "Annual receipts ($1,000, with noise)"

merge m:1 sect using ../../data/interim/xwalk_sect_naics.dta, keepusing(naics2) keep(1 3) nogen
order sect naics2 class

compress
save ../../data/processed/activity_universe_2019.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  end activity_universe_2019.do"

if "$S_CONSOLE" == "console" & "$batch" != "1" exit, STATA clear
