if "$draw_charts" != "1" {
   exit
}

clear all

use ../../data/interim/esize_rcpt5b.dta, clear

replace lesize = ln(esize_rcpt5b) if lesize == . & esize_rcpt5b != .


// get list of sectors
sum sect
local first_sect = r(min)
local last_sect = r(max)


local pdflist ""
forvalues i = `first_sect'/`last_sect' {
   local sect: label l_sect `i'

   // find value of empl-by-rcpt CDF at $5b
   sum rcptcdf_empl_rcpt5b if sect == `i' & ilesize == ilesize_rcpt5b
   // not all sectors have firm with $5b in rcpt, skip if none
   if r(N) > 0 {
      local rcptcdf_rcpt5b = r(mean)

      noi disp "drawing `sect' (`i')"

      sum esize_rcpt5b if sect == `i' & ilesize == ilesize_rcpt5b
      local esize_rcpt5b = r(mean)
      local lesize_rcpt5b = ln(`esize_rcpt5b')
      local esize_rcpt5b = string(`esize_rcpt5b', "%6.0fc")


      #delim ;
      tw
      (scatter emplcdf_empl lesize if esize_rcpt5b == 14999, yaxis(1 2) msy(Dh) msize(medsmall) lw(thin) col("$c10") mfc("$c10%50"))
      (scatter emplcdf_empl lesize if esize_rcpt5b != 14999, yaxis(1 2) msy(O) msize(medsmall) lw(thin) col("$c7") mfc("$c7%50"))
      (line emplcdf_empl_rcpt5b lesize, yaxis(1 2) lw(medthick) col("$c8%55"))
      (pci `rcptcdf_rcpt5b' 0 `rcptcdf_rcpt5b' `lesize_rcpt5b', m(i) c(l) lc("$c9") lw(thin) lp(shortdash))
      (pci 0 `lesize_rcpt5b' `rcptcdf_rcpt5b' `lesize_rcpt5b', m(i) c(l) lc("$c9") lw(thin) lp(shortdash))
      if sect == `i',
      ylab(0(.2)1, for(%3.1f) labs(9pt)) ytitle("") ymtick(1, nolab tlen(4))
      ylab(0(.2)1, nolab axis(2)) ytitle("", axis(2)) ymtick(1, axis(2) nolab tlen(4))
      text(.06 `lesize_rcpt5b' " `esize_rcpt5b'", col("$c9") size(8pt) place(e))
      title("{bf:Employment CDF by firm employment size}" "`sect'", size(9pt))
      subtitle("Fraction of activity", pos(11) size(9pt))
      xtitle("Firm size, employment (log scale)", size(9pt))
      xlab(
      `=ln(1e1)' "10"
      `=ln(1e2)' "10{sup:2}"
      `=ln(1e3)' "10{sup:3}"
      `=ln(1e4)' "10{sup:4}"
      `=ln(1e5)' "10{sup:5}"
      `=ln(1e6)' "10{sup:6}",
      labgap(4pt) labs(9pt))
      xmtick(
      0
      .69314718
      1.0986123
      1.3862944
      1.6094379
      1.7917595
      1.9459101
      2.0794415
      2.1972246
      2.3025851
      2.9957323
      3.4011974
      3.6888795
      3.912023
      4.0943446
      4.2484952
      4.3820266
      4.4998097
      4.6051702
      5.2983174
      5.7037825
      5.9914645
      6.2146081
      6.3969297
      6.5510803
      6.6846117
      6.8023948
      6.9077553
      7.6009025
      8.0063676
      8.2940496
      8.5171932
      8.6995147
      8.8536654
      8.9871968
      9.1049799
      9.2103404
      9.9034876
      10.308953
      10.596635
      10.819778
      11.0021
      11.156251
      11.289782
      11.407565
      11.512925
      12.206073
      12.611538
      12.89922
      13.122363
      13.304685
      13.458836
      13.592367
      13.71015
      13.815511
      14.508658
      14.914123,
      tlen(*.8))
      xsca(r(0 15))
      legend(order(2 "SUSB data"
                  1 "15,000 empl"
                  3 "Fitted empl-size CDF"
                  4 "y-axis: Firms w/ receipts < $5b")
            size(8pt) ring(1) pos(6) col(2) colfirst)
      aspect(.618) ysize(5.2in)
      ;
      #delim cr

      if "$S_CONSOLE" == "console" {
         graph export ../../output/esize_`i'.pdf, replace fontdir(../../lib/fonts) fontface("Charter")
         local pdflist = "`pdflist' esize_`i'.pdf"
      }
   }
}

if "$S_CONSOLE" == "console" {
   cd ../../output
   !gs -q -dSAFER -dBATCH -dNOPAUSE -sOutputFile=esize_rcpt5b.pdf -sDEVICE=pdfwrite -f `pdflist'
   !rm -f `pdflist' >&/dev/null
   cd ../code/analysis
}
