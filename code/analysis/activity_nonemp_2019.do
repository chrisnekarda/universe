// activity_nonemp_2019.do
//
// Scales up 2017 farms activity to 2019
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin activity_nonemp_2019.do"

tempfile gdp susb

// GDP FACTORS BY SECTOR //
use ../../data/interim/va_by_industry.dta, clear
tsset sect qdate, q
tscollap gdp, to(y) gen(year)
keep if year == 2017 | year == 2019
reshape wide gdp, i(sect) j(year)
gen gdp_ratio = gdp2019/gdp2017
keep sect gdp_ratio
save `gdp'


// SUSB RATIOS //
use ../../data/processed/activity_susb_ebin_2019.dta, clear

// sum payroll and receipts for small firms
collapse (sum) payr rcpt if esize > 0 & esize <= 499, by(sect)

gen payr_frac = payr / rcpt

keep sect payr_frac

save `susb'



// NONEMPLOYERS //
use ../../data/interim/nonemp.dta, replace
drop st

// merge in sect and keep only 2-digit
merge m:1 naics using ../../data/interim/xwalk_sect_naics.dta, keepusing(sect) keep(3) nogen

keep if lfo == "-"
drop lfo

keep if rcptot_size == 1
drop rcptot_size

rename estab estb
rename rcptot rcpt

format estb rcpt %16.0fc

// merge in value added by sector
merge m:1 sect using `gdp', keep(1 3) nogen

// merge in payroll ratio by sector
merge m:1 sect using `susb', keep(1 3) nogen


// scale up measures to 2019 terms //

// scale up receipts using GDP ratio
replace rcpt = rcpt * gdp_ratio   // NIPA

// uninc self-employment was unchanged from 2017:Q1 to 2019:Q1
// use quarterly because March to March is too noisy in CPS
// (https://fred.stlouisfed.org/graph/?g=xI3d)
replace estb = estb * 1.003

// create firms and empl equal to estabs
gen firm = estb
gen empl = estb

// use ratio of payroll to receipts from SUSB <$5 firms
gen payr = rcpt * payr_frac

// round
foreach xx in firm estb empl payr rcpt {
   replace `xx' = round(`xx')
}

format firm estb empl payr rcpt %16.0fc

keep sect firm estb empl payr rcpt


// merge in sect and keep only 2-digit
merge m:1 sect using ../../data/interim/xwalk_sect_naics.dta, keepusing(naics2) keep(3) nogen

order sect naics2 firm estb empl payr rcpt

// label variables
label var naics2 "Industry code"
label var firm "Number of firms"
label var estb "Number of establishments"
label var empl "Employment (with noise)"
label var payr "Annual payroll ($1,000, with noise)"
label var rcpt "Estimated receipts ($1,000, with noise)"

compress
save ../../data/processed/activity_nonemp_2019.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  end activity_nonemp_2019.do"
