// interp_empl15k.do
//
// Estimate 15,000-employee cut-off in 2017 SUSB emplsize distribution
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

// variable for date-time stamp
local d = string(clock("`c(current_date)' `c(current_time)'", "DMY hms"), "%tcCCYYNNDD!THHMMSS")

cap log close
log using ../../log/interp_empl15k_`d'.log, text replace

noi disp as text "$S_DATE $S_TIME  begin interp_empl15k.do"
est drop _all

tempfile cdf_top pre_interp


// load employment at largest firm by sector (from Compustat)
tempvar maxmax
use ../../data/interim/cdf_top_2017.dta, clear
drop top_rcpt
gen byte ebin = 18
rename top_empl esize

// in agriculture (sect 2), max emp is 17,475, but there is activity in the
// SUSB 20,000+ bin. using the reported employment (23,330) without
// reducing it by the domestic scaling factor (.75). This is close to the
// next smallest top employer (utilies, ~26,000).
replace esize = 23300 if sect == 2

// create a total category which is the global maximum
egen `maxmax' = max(esize)
expand 2 in 1
replace sect = 1 in l
replace esize = `maxmax' in l
drop `maxmax'
sort sect

save `cdf_top'


// Start with 2017 SUSB emplsize data
use ../../data/processed/susb_emplsize_2017.dta, clear

drop *fl*
rename empl_n empl
rename payr_n payr
rename rcpt_n rcpt
rename entrsize ebin
rename maxsize esize

// drop the subtotal categories
drop if ebin == 5 | ebin == 8 | ebin == 9

// replace total category with a zero
replace esize = 0 if ebin == 1

// merge in 2017 top firm size data from Compustat
merge 1:1 sect ebin using `cdf_top', update nogen
sort sect ebin

label val ebin .

// compute empirical CDFs (excluding total category)
foreach xx in firm estb empl payr rcpt {
   bys sect: gen cum = sum(`xx') if esize != 0
   egen tot = max(cum), by(sect)
   gen emplcdf_`xx' = cum / tot
   drop cum tot
}

// we don't use the largest size (20,000+), but need to re-assign
// these bin codes
replace ebin = 19 if ebin == 18
replace ebin = 18 if ebin == 17

save `pre_interp'
/* save pre-interp.dta, replace */

// get list of sectors
sum sect
local first_sect = r(min)
local last_sect = r(max)


// estimating the 15,000 employee cut-off is done separately for each sector
local pdflist ""
cap erase ../../data/interim/interp_empl15k.dta

forvalues i = `first_sect'/`last_sect' {
/* foreach i in 1 2 3 6 8 18 { */
   local sect: label l_sect `i'

   use `pre_interp', clear

   sum firm if sect == `i'
   if r(N) > 0 {

      // keep only sector `i'
      keep if sect == `i'

      noi disp _n "-------------------------------------------------------------------------------"
      noi disp "`sect'"

      // add a bin for the 15,000 cut-off
      set obs `=_N + 1'
      replace sect = sect[1] in l
      replace esize = 14999 in l
      replace ebin = 17 in l
      sort ebin

      gen lesize = ln(esize)
      order lesize, after(esize)
      label var lesize "Firm size (employment, log)"

      // create index for plotting the CDF approximation
      gen ilesize = ceil(250 * lesize)
      replace ilesize = 0 if esize == 0
      order sect ilesize
      tsset sect ilesize
      tsfill

      // drop obs very far away from interpolation range
      drop if ebin == . & esize < 499

      // fill in esize from index
      replace esize = round(exp(ilesize/250)) if esize == .

      // can't have finer size bins than 1 employee!
      egen foo = max(ilesize), by(sect esize)
      keep if ilesize == foo
      drop foo

      // fill in log esize
      replace lesize = ln(esize) if lesize == .

      gen lesize2 = lesize^2
      order lesize2, after(lesize)


      // FIRMS ARE SPECIAL //
      noi disp _n "Activity measure: firm"

      // quadratic
      noi reg emplcdf_firm lesize lesize2 if esize >= 4999
      predict emplcdf_firm_hat_quad if esize >= 4999, xb

      // beta (Logistic)
      clonevar emplcdf_firm2 = emplcdf_firm
      replace emplcdf_firm2 = emplcdf_firm - 1e-7 if emplcdf_firm == 1
      // use quadratic in lesize
      noi betareg emplcdf_firm2 lesize lesize2 if esize >= 4999, nolog
      predict emplcdf_firm_hat_beta if esize >= 4999

      // check that first derivative of fitted CDF doesn't change sign
      noi sum D1.emplcdf_firm_hat_beta
      if r(min) < 0 & r(max) > 0 {
         // it did, use linear formulation
         drop emplcdf_firm_hat_beta
         noi betareg emplcdf_firm2 lesize if esize >= 4999, nolog
         predict emplcdf_firm_hat_beta if esize >= 4999
      }
      drop emplcdf_firm2


      // pick which estimate fits best
      gen emplcdf_firm_hat = .

      // check that CDF from quadratic is strictly smaller than 1 at 15,000
      sum emplcdf_firm_hat_quad if esize == 14999
      local quad_at_empl15k = r(mean)

      noi sum D1.emplcdf_firm_hat_quad
      local quad_rmin = r(min)
      local quad_rmax = r(max)

      // compute RMSE of quadratic and beta
      noi disp _n "RMSE:"
      noi rmse emplcdf_firm emplcdf_firm_hat_quad emplcdf_firm_hat_beta if esize >= 4999

      // use the quadratic if RMSE is lower *and* estimate < 1 *and* slope of
      // quadratic doesn't change sign
      if real(r(emplcdf_firm_hat_quad)) < real(r(emplcdf_firm_hat_beta)) & ///
         `quad_at_empl15k' < 1 & ///
         !(`quad_rmin' < 0 & `quad_rmax' > 0) {
         replace emplcdf_firm_hat = emplcdf_firm_hat_quad
         local quad_wgt = "medthick"
         local beta_wgt = "medthin"
         local quad_col = "$c8%60"
         local beta_col = "$c9%40"
      }
      // otherwise use beta
      else {
         replace emplcdf_firm_hat = emplcdf_firm_hat_beta
         local quad_wgt = "medthin"
         local beta_wgt = "medthick"
         local quad_col = "$c8%40"
         local beta_col = "$c9%60"
      }

      gen emplcdf_firm_empl15k = emplcdf_firm_hat if esize == 14999


      // ALL MEASURES BUT FIRMS USE ONLY QUADRATIC //
      foreach xx in estb empl payr rcpt {
         noi disp _n "Activity measure: `xx'"

         // quadratic
         noi reg emplcdf_`xx' lesize lesize2 if esize >= 4999
         predict emplcdf_`xx'_hat if esize >= 4999, xb

         gen emplcdf_`xx'_empl15k = emplcdf_`xx'_hat if esize == 14999
      }

      if "$draw_charts" == "1" {
         #delim ;

         tw
         (scatter emplcdf_firm lesize if ebin != ., yaxis(1 2) msym(O) col("$c7") mfc("$c7%75"))
         (line emplcdf_firm_hat_quad lesize, yaxis(1 2) lw(medthick) col("$c8%55"))
         (line emplcdf_firm_hat_beta lesize, yaxis(1 2) lw(medthick) col("$c9%55"))
         (scatter emplcdf_firm_empl15k lesize, yaxis(1 2) msym(dh) col("$c10"))
         if sect == 0 & esize >= 1999,
         xlab(7(1)15, labc(white) labsize(9pt))
         xtitle("")
         ylab(0(.2)1, for(%-7.4g) labc(white) labsize(9pt)) ymtick(1, nolab tlen(5))
         ylab(0(.2)1, axis(2) nolab) ymtick(1, axis(2) nolab tlen(5))
         legend(order(1 "Actual" 2 "Fitted - Quadratic" 3 "Fitted - Logistic" 4 "15,000 cut-off") size(9pt))
         title("{bf:Legend}", size(9pt))
         graphr(m(0 2 0 0))
         name(legd, replace)
         ;

         tw
         (scatter emplcdf_firm lesize if ebin != ., yaxis(1 2) msym(O) col("$c7") mfc("$c7%75"))
         (line emplcdf_firm_hat_quad lesize, yaxis(1 2) lw(`quad_wgt') col("`quad_col'"))
         (line emplcdf_firm_hat_beta lesize, yaxis(1 2) lw(`beta_wgt') col("`beta_col'"))
         (scatter emplcdf_firm_empl15k lesize, yaxis(1 2) msym(dh) col("$c10"))
         if esize >= 1999,
         xlab(7(1)15, labsize(9pt))
         xtitle("")
         ylab(#7, for(%-7.4g) labsize(9pt)) ymtick(1, nolab tlen(5))
         ylab(#7, axis(2) nolab) ymtick(1, axis(2) nolab tlen(5))
         legend(off)
         title("{bf:Firm count}", size(9pt))
         graphr(m(0 2 0 0))
         name(firm, replace)
         ;

         tw
         (scatter emplcdf_estb lesize if ebin != ., yaxis(1 2) msym(O) col("$c7") mfc("$c7%75"))
         (line emplcdf_estb_hat lesize, yaxis(1 2) lw(medthick) col("$c8%55"))
         (scatter emplcdf_estb_empl15k lesize, yaxis(1 2) msym(dh) col("$c10"))
         if esize >= 1999,
         xlab(7(1)15, labsize(9pt))
         xtitle("")
         ylab(#7, for(%-7.4g) labsize(9pt)) ymtick(1, nolab tlen(5))
         ylab(#7, axis(2) nolab) ymtick(1, axis(2) nolab tlen(5))
         legend(off)
         title("{bf:Establishment count}", size(9pt))
         graphr(m(0 2 0 0))
         name(estb, replace)
         ;

         tw
         (scatter emplcdf_empl lesize if ebin != ., yaxis(1 2) msym(O) col("$c7") mfc("$c7%75"))
         (line emplcdf_empl_hat lesize, yaxis(1 2) lw(medthick) col("$c8%55"))
         (scatter emplcdf_empl_empl15k lesize, yaxis(1 2) msym(dh) col("$c10"))
         if esize >= 1999,
         xlab(7(1)15, labsize(9pt))
         xtitle("")
         ylab(#7, for(%-7.4g) labsize(9pt)) ymtick(1, nolab tlen(5))
         ylab(#7, axis(2) nolab) ymtick(1, axis(2) nolab tlen(5))
         legend(off)
         title("{bf:Employment}", size(9pt))
         graphr(m(0 2 0 0))
         name(empl, replace)
         ;

         tw
         (scatter emplcdf_payr lesize if ebin != ., yaxis(1 2) msym(O) col("$c7") mfc("$c7%75"))
         (line emplcdf_payr_hat lesize, yaxis(1 2) lw(medthick) col("$c8%55"))
         (scatter emplcdf_payr_empl15k lesize, yaxis(1 2) msym(dh) col("$c10"))
         if esize >= 1999,
         xlab(7(1)15, labsize(9pt))
         xtitle("")
         ylab(#7, for(%-7.4g) labsize(9pt)) ymtick(1, nolab tlen(5))
         ylab(#7, axis(2) nolab) ymtick(1, axis(2) nolab tlen(5))
         legend(off)
         title("{bf:Annual payroll}", size(9pt))
         graphr(m(0 2 0 0))
         name(payr, replace)
         ;

         tw
         (scatter emplcdf_rcpt lesize if ebin != ., yaxis(1 2) msym(O) col("$c7") mfc("$c7%75"))
         (line emplcdf_rcpt_hat lesize, yaxis(1 2) lw(medthick) col("$c8%55"))
         (scatter emplcdf_rcpt_empl15k lesize, yaxis(1 2) msym(dh) col("$c10"))
         if esize >= 1999,
         xlab(7(1)15, labsize(9pt))
         xtitle("")
         ylab(#7, for(%-7.4g) labsize(9pt)) ymtick(1, nolab tlen(5))
         ylab(#7, axis(2) nolab) ymtick(1, axis(2) nolab tlen(5))
         legend(off)
         title("{bf:Annual receipts}", size(9pt))
         graphr(m(0 2 0 0))
         name(rcpt, replace)
         ;

         graph combine legd firm estb empl payr rcpt,
         title("{bf:Emprical and Fitted CDFs}" "`sect'", size(9pt))
         note("Log firm employment size", pos(6) size(8pt))
         ysize(3.0in)
         ;
         #delim cr

         graph drop legd firm estb empl payr rcpt

         if "$S_CONSOLE" == "console" {
            graph export ../../output/emplcdf_`i'.pdf, replace fontdir(../../lib/fonts) fontface("Charter")
            local pdflist = "`pdflist' emplcdf_`i'.pdf"
         }
      }

      // save interpolation results
      keep sect ilesize ebin esize lesize emplcdf*

      cap confirm file ../../data/interim/interp_empl15k.dta
      if !_rc {
         noi merge 1:1 sect ilesize using ../../data/interim/interp_empl15k.dta, nogen
         sort sect ilesize
         compress
         save ../../data/interim/interp_empl15k.dta, replace
      }
      else {
         compress
         save ../../data/interim/interp_empl15k.dta
      }

      noi disp "-------------------------------------------------------------------------------"
   }
}

// put together chart packet if it was created
if "$S_CONSOLE" == "console" & "`pdflist'" != "" {
   cd ../../output
   !gs -q -dSAFER -dBATCH -dNOPAUSE -sOutputFile=interp_empl15k.pdf -sDEVICE=pdfwrite -f `pdflist'
   !rm -f `pdflist' >&/dev/null
   cd ../code/analysis
}



// start from pre-interp activity and CDF measures //
use `pre_interp', clear

// merge in estimated 15,000 empl location in CDF, where applicable
merge 1:m sect ebin using ../../data/interim/interp_empl15k.dta, nogen keepusing(esize emplcdf*_empl15k) update
sort sect ebin

// drop the super-fine bins used for plotting the CDF estimates
drop if ebin == .


// fill in the 15,000 location in the CDF using the best estimate
foreach xx in firm estb empl payr rcpt {
   replace emplcdf_`xx' = emplcdf_`xx'_empl15k if esize == 14999 & emplcdf_`xx' == .
   drop emplcdf_`xx'_empl15k
}


// distribute activity estimates from [10,000, 20,000) bin to
// new [10,000, 15,000) and [15,000, 20,000) bins
foreach xx in firm estb empl payr rcpt {
   // this picks up the "total" bin
   egen tot = max(`xx'), by(sect)

   // translate CDF back into activity measure
   gen cum  = round(emplcdf_`xx' * tot)

   // contribution of each ebin to the total
   // these match original for all but the [10,000, 20,000) bins
   bys sect (ebin): gen marg = cum[_n] - cum[_n-1] if esize != 0

   // replace the activty measure with the new values in the new bins
   replace `xx' = marg if esize >= 14999

   drop tot cum marg
}

/*
// check that the new measures sum to the total
foreach xx in firm estb empl payr rcpt {
   bys sect: gen cum = sum(`xx') if esize != 0
   egen chk = max(cum), by(sect)
   egen tot = max(`xx'), by(sect)
   gen `xx'_dif = tot - chk
   drop cum chk tot
}
noi sum *_dif
drop *_dif

// they do!
*/

// clean up
keep sect ebin esize firm estb empl payr rcpt

merge m:1 sect using ../../data/interim/xwalk_sect_naics.dta, keepusing(naics2) keep(1 3) nogen
order sect naics2

format firm estb empl payr rcpt %16.0fc

// label data
label var naics2 "Industry code"
label var esize "Firm size (employment)"
label var firm "Number of firms"
label var estb "Number of establishments"
label var empl "Employment (with noise)"
label var payr "Annual payroll ($1,000, with noise)"
label var rcpt "Estimated receipts ($1,000, with noise)"

compress
save ../../data/processed/susb_emplsize_2017_empl15k.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  end interp_empl15k.do"
cap log close

if "$S_CONSOLE" == "console" & "$batch" != "1" exit, STATA clear
