// analysis.do
//
// Main script for paper analysis
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

set more off

// variable for date-time stamp
local d = string(clock("`c(current_date)' `c(current_time)'", "DMY hms"), "%tcCCYYNNDD!THHMMSS")

cap log close
log using ../../log/analysis_`d'.log, text replace

noi disp as text "$S_DATE $S_TIME  begin analysis.do"

// project-wide Stata settings
if "$proj_config" != "1" {
   noi run ../_config.do
}

// run programs as part of main script? (0 = no, 1 = yes)
global batch = 1

// draw charts? (0 = no, 1 = yes)
// note PDFs will only be saved if Stata is run from the console
if "$draw_charts" == "" {
   global draw_charts = 1
}


// CALCULATIONS USING SUSB //

// estimate a $5 B cut-off in 2017 SUSB rcptsize file and distribute
// activity from [2.5 B, ∞) bin to new [2.5 B, 5 B) and [5 B, ∞) bins
noi run interp_rcpt5b.do

// estimate a 15,000 employee cut-off in 2017 SUSB emplsize file and
// distribute activity estimates from [10,000, 20,000) bin to new
// [10,000, 15,000) and [15,000, 20,000) bins
noi run interp_empl15k.do

// estimate where $5b rcpt cut-off is in 2017 SUSB emplsize distribution
noi run esize_rcpt5b.do
noi run plot_esize_rcpt5b.do

// calculate activity shares by LFO
noi run activity_shares_lfo.do

// collapse activity to SUSB ebin and scale up to 2019 levels
noi run activity_susb_ebin_2019.do

// calculate 2019 SUSB activity by LFO and major size class
noi run activity_susb_lfo_2019.do

// calculate aggregate firm count correction
noi run firm_dup.do



// PROCESS NON SUSB DATA //

// process railroads activity
noi run activity_railroads_2019.do

// scale up farms activity to 2019
noi run activity_farms_2019.do

// scale up nonemployers activity to 2019
noi run activity_nonemp_2019.do


// AGGREGATE UNIVERSE OF ACTIVITY BY SECTOR, SIZE, AND LFO //
noi run activity_universe_2019.do


// ESTIMATE ACTIVITY IN NAICS 72 ELIGIBLE FOR PPP //
noi run activity_naics72_ppp.do


// CREATE TABLES //

// create table for activity measures
noi run tab_activity.do


noi disp as text "$S_DATE $S_TIME  end analysis.do"
cap log close

if "$S_CONSOLE" == "console" & "$batch" != "1" exit, STATA clear
