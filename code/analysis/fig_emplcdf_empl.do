use ../../data/processed/susb_emplsize_2017_empl15k.dta, clear

// compute empirical CDFs (excluding total category)
foreach xx in firm estb empl payr rcpt {
   bys sect: gen cum = sum(`xx') if esize != 0
   egen tot = max(cum), by(sect)
   gen emplcdf_`xx' = cum / tot
   drop cum tot
}


gen lesize = ln(esize)


keep if sect == 1


# delim ;
tw
(scatter emplcdf_empl lesize if ebin == 17, yaxis(1 2) msy(Dh) msize(medlarge) col("$c10") mfc("$c10%50"))
(scatter emplcdf_empl lesize if ebin != 17, yaxis(1 2) msy(O) msize(medlarge) col("$c7") mfc("$c7%50")),
subtitle("Fraction of activity", pos(11) size(9pt))
ylab(0(.2)1, for(%3.1f)) ytitle("") ymtick(1, nolab tlen(4))
ylab(0(.2)1, nolab axis(2)) ytitle("", axis(2)) ymtick(1, axis(2) nolab tlen(4))
xtitle("Firm size, employment (log scale)", size(9pt))
/*xlab(
`=ln(1e1)' "10"
`=ln(1e2)' "100"
`=ln(1e3)' "1,000"
`=ln(1e4)' "10,000"
`=ln(1e5)' "100,000"
`=ln(1e6)' "1,000,000",
labs(9pt))*/
xlab(
`=ln(1e1)' "10"
`=ln(1e2)' "10{sup:2}"
`=ln(1e3)' "10{sup:3}"
`=ln(1e4)' "10{sup:4}"
`=ln(1e5)' "10{sup:5}"
`=ln(1e6)' "10{sup:6}",
labgap(4pt)
labs(9pt))
xmtick(
0
.69314718
1.0986123
1.3862944
1.6094379
1.7917595
1.9459101
2.0794415
2.1972246
2.3025851
2.9957323
3.4011974
3.6888795
3.912023
4.0943446
4.2484952
4.3820266
4.4998097
4.6051702
5.2983174
5.7037825
5.9914645
6.2146081
6.3969297
6.5510803
6.6846117
6.8023948
6.9077553
7.6009025
8.0063676
8.2940496
8.5171932
8.6995147
8.8536654
8.9871968
9.1049799
9.2103404
9.9034876
10.308953
10.596635
10.819778
11.0021
11.156251
11.289782
11.407565
11.512925
12.206073
12.611538
12.89922
13.122363
13.304685
13.458836
13.592367
13.71015
13.815511
14.508658
14.914123,
tlen(*.8)
)
xsca(r(0 15))
legend(order(2 "SUSB data" 1 "15,000-employee cut-off") size(8pt))
scheme(texpdf)
aspect(.618) xsize(3) ysize(2.25)
;

graph export ../../output/fig_empl_cdf.pdf,
replace
fontdir(../../lib/fonts)
fontface("Charter")
;
#delim cr


/*
// code to generate the minor ticks
set obs 8
gen n = _n
gen y = 10^(n-1)
gen n9 = 9*n
tsset n9
tsfill
ipolate y n9, gen(y9)
gen xmtick = ln(y9)
list xmtick if xmtick<15, clean noobs
*/


/* xlab(
1.3862944 "5"
2.1972246 "10"
2.9444390 "20"
4.5951199 "100"
6.2126061 "500"
/*6.6187390 "750"*/
6.9067548 "1,000"
/*7.3125535 "1,500"*/
/*7.6004023 "2,000"*/
/*7.8236459 "2,500"*/
8.5169932 "5,000"
9.2102404 "10,000"
9.9034376 "20,000"
14.360738 "1,725,000"
, labs(medsmall)
alt) */
/* xmtick(`=ln(15000)', tl(small) tlw(medthin) tlcolor("$c10")) */
