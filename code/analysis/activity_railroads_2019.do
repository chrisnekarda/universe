// activity_railroads_2019.do
//
// "Import" and process railroads data -- mostly hard coded from PDFs
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin activity_railroads_2019.do"

tempfile qcew qcew2017 qcew2019 susbrr classIrcpt classIempl


// Calculate ratio of payr/empl in 2017 and 2019 QCEW data
// used to gross up payroll from 2017 to 2019
foreach yyyy in 2017 2019 {
   insheet using ../../data/raw/`yyyy'_qtrly_singlefile.csv, comma clear

   // keep NAICS 48-49
   keep if industry_code == "48-49"

   // keep only private
   keep if own_code == 5

   // only want March employment
   replace month3_emplvl = 0 if qtr != 1

   // compute sum
   collapse (sum) month3_emplvl total_qtrly_wages, by(year)

   // payroll per employee
   gen payr_empl`yyyy' = (total_qtrly_wages/1000)/month3_emplvl
   keep year payr_empl`yyyy'

   compress
   save `qcew`yyyy''
}

use `qcew2017'
replace year = 2019
merge 1:1 year using `qcew2019', nogen
drop year

gen payr_empl_growth_qcew = payr_empl2019/payr_empl2017

keep *_growth_qcew

gen naics = 482

compress
save `qcew'



// receipts at large Class I railroads //
// 2019 revenue in 1,000s
// Source: Surface Transportation Board

import excel "../../data/raw/EARN-COMP-2019-Q4.xls", sheet("Output") cellrange(A15:K31) clear firstrow case(lower)

keep if year == 2019
rename h rcpt_classI

replace railroad = "bnsf" if railroad == "Burlington Northern - Santa Fe"
replace railroad = "csx" if railroad == "CSX Transportation"
replace railroad = "cngt" if railroad == "CN/Grand Trunk Corporation"
replace railroad = "kcs" if railroad == "Kansas City Southern"
replace railroad = "ns" if railroad == "Norfolk Southern"
replace railroad = "soo" if railroad == "Soo Line"
replace railroad = "up" if railroad == "Union Pacific"

keep railroad year rcpt_classI

gen size = .
replace size = 3 if railroad == "bnsf"
replace size = 2 if railroad == "cngt"
replace size = 3 if railroad == "csx"
replace size = 2 if railroad == "kcs"
replace size = 3 if railroad == "ns"
replace size = 2 if railroad == "soo"
replace size = 3 if railroad == "up"

drop if size == .

collapse (sum) rcpt_classI, by(size)

gen naics = 482

save `classIrcpt'


// employment at Class I railroads
// March 2019 employment
// Source: Surface Transportation Board

import excel "../../data/raw/March 2019 Employment Compilation 2000 Index.xls", sheet("Input") cellrange(A6) clear firstrow case(lower)
drop employeesbycompanyandjobdes

keep if b == td(01mar2019)

rename ay emplbnsf
rename az emplcsx
rename ba emplcngt
rename bb emplkcs
rename bc emplns
rename bd emplsoo
rename be emplup

gen naics = 482

keep naics empl*

reshape long empl, i(naics) j(railroad) str

rename empl empl_classI

gen size = .
replace size = 3 if railroad == "bnsf"
replace size = 2 if railroad == "cngt"
replace size = 3 if railroad == "csx"
replace size = 2 if railroad == "kcs"
replace size = 3 if railroad == "ns"
replace size = 2 if railroad == "soo"
replace size = 3 if railroad == "up"

drop if size == .

collapse (sum) empl_classI, by(size)

gen naics = 482

save `classIempl'



// Get size-specific ratios for transport sector from SUSB
use ../../data/processed/susb_emplsize_2017.dta, clear

rename empl_n empl
rename payr_n payr
rename rcpt_n rcpt

// keep transportation and warehousing
keep if sect == 9

// drop the totals/subtotals
drop if entrsize == 1 | entrsize == 5 | entrsize == 8 | entrsize == 9


// small is empl < 1,500
gen size = 1 if entrsize <= 12
// medium is empl 1,500 - 19,999
replace size = 2 if entrsize >= 13 & entrsize <= 17
// large is empl >= 20,000
replace size = 3 if entrsize == 18

collapse (sum) firm estb empl payr rcpt, by(size)

// estabs per firm
gen estb_firm = estb/firm
// payroll per worker
gen payr_empl = payr/empl
// receipts per worker
gen rcpt_empl = rcpt/empl

foreach xx in empl payr rcpt {
   egen `xx'_sm_share = pc(`xx') if size < 3, prop
}

compress
save `susbrr'



// FIRMS //

// firm count in 2018 is most current

// Get firm count from list of employers
import excel "../../data/raw/TotalEmployment2018.xls", sheet("TotalEmployment") cellrange(A5) firstrow case(lower) clear
keep railroademployer
sort railroademployer
duplicates drop railroademployer, force
drop if railroademployer == ""
count
gen firm = r(N)
keep in 1
keep firm

// RRB list of firms does not include Amtrak
replace firm = firm + 1

// from STB, there are 4 Class I railroads with empl > 15,000 and rcpt > $5b
gen firm3 = 4

// there are "about 20" Class II railroads, plus 4 Class I RRs with
// empl < 15,000 or rcpt < $5 bn, plus Amtrak
gen firm2 = 20 + 4 + 1

// the remainder of the firms are small
gen firm1 = firm - firm3 - firm2

drop firm
gen naics = 482

reshape long firm, i(naics) j(size)



// ESTABS //

// merge in SUSB ratios
merge 1:1 size using `susbrr', nogen keepusing(estb_firm)

// calculate RR estabs using SUSB estb/firm by size from NAICS 48-49
gen estb = round(firm * estb_firm)
drop estb_firm



// EMPL //

// merge in SUSB ratios
merge 1:1 size using `susbrr', nogen keepusing(empl_sm_share)

reshape wide firm estb empl_sm_share, i(naics) j(size)

// 2019 employment
// from selectdt.pdf; Railroad Labor Force (thousands), employed,
// March 2019 value 221,000

// Employment in large (131,625) comes from STB Class 1
gen empl3 = 131625

// medium comes from share of medium employment
gen empl2 = round((221000 - empl3) * empl_sm_share2)

// small is residual so it adds up exactly
gen empl1 = 221000 - empl2 - empl3

drop empl_sm_share*

reshape long firm estb empl, i(naics) j(size)



// PAYR //
merge 1:1 size using `susbrr', nogen keepusing(payr_empl)

// 2017 payroll in 1,000s
// from selectdt.pdf; Railroad Payroll ($billions), total
gen payr_2017 = 19.7 * 1e6

// gross up to 2019 using QCEW payroll ratio
merge m:1 naics using `qcew', nogen keepusing(payr_empl_growth_qcew)
// gross-up factor of payroll = (2019/17 ratio of payroll/employee) from QCEW *
// (2019/17 ratio of employment) from RRB (selectdt.pdf)
gen payr = payr_2017 * (payr_empl_growth_qcew) * (214/225)
drop payr_empl_growth_qcew payr_2017

// share out using payr/empl (payr_empl)
gen foo = empl * payr_empl
egen payr_empl_share = pc(foo), prop
drop foo payr_empl

replace payr = round(payr * payr_empl_share)
drop payr_empl_share



// RCPT //

merge 1:1 size naics using `classIrcpt', nogen
merge 1:1 size naics using `classIempl', nogen

gen foo = rcpt_classI/empl_classI if size == 2
egen rcpt_empl_classI = max(foo)
drop foo

gen rcpt = round(rcpt_empl_classI * empl) if size == 1 | size == 2
replace rcpt = rcpt_classI if size == 3

drop *classI


keep naics size firm estb empl payr rcpt
order naics size firm estb empl payr rcpt
format firm estb empl payr rcpt %16.0fc

// label variables
label var naics "Industry code"
label var firm "Number of firms"
label var estb "Number of establishments"
label var empl "Employment"
label var payr "Annual payroll ($1,000)"
label var rcpt "Estimated receipts ($1,000)"

compress
save ../../data/processed/activity_railroads_2019.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  begin activity_railroads_2019.do"

if "$S_CONSOLE" == "console" & "$batch" != "1" exit, STATA clear
