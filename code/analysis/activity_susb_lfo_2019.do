// activity_susb_lfo_2019.do
//
// Calculate measures of activity from SUSB data by size (small, medium,
// large) and LFO (for-profit, nonprofit, govt)
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin activity_susb_lfo_2019.do"


tempfile check lfo


// check totals
use ../../data/processed/activity_susb_ebin_2019.dta, clear
keep if ebin == 1
keep sect firm estb empl payr rcpt
save `check'


// activity by LFO3
use ../../data/processed/activity_shares_lfo.dta, clear

// keep the empl<500 (entrsize=8) and empl>=500 (entrsize=9) categories
keep if entrsize == 8 | entrsize == 9

// create the 3-state size class variable
// use the 500+ LFO split for the medium and large size classes
gen byte size = .
replace size = 1 if entrsize == 8
replace size = 3 if entrsize == 9
order sect size lfo3

keep sect size lfo3 *shr

// create a medium size class (size=2) that is a copy of large (size=3)
expand 2 if size == 3, gen(exp)
replace size = 2 if size == 3 & exp == 1
drop exp
sort sect size lfo3

save `lfo'



// divide SUSB activity into small, medium, and large size classes
use ../../data/processed/activity_susb_ebin_2019.dta, clear
sort sect esize

// compute empirical CDFs (excluding total category)
foreach aa in firm estb empl payr rcpt {
   bys sect (esize): gen cum = sum(`aa') if esize != 0
   egen tot = max(cum), by(sect)
   gen emplcdf_`aa' = cum / tot
   drop cum tot
}



gen byte size = 0
order sect naics2 size

// SIZE CLASSIFICATIONS //

// EMPLOYMENT-BASED SIZE CRITERIA //
// esize_rcpt5b is the firm employment size of firms with $5b in receipts

// SMALL: firms with [1, 500) employees
replace size = 1 if esize > 0 & esize <= 499

// MEDIUM: firms with [500, max(15,000, esize_rcpt5b)) employees
replace size = 2 if esize > 499 & esize <= 14999

// LARGE: firms with more than max(15,000, esize_rcpt5b) employees
replace size = 3 if esize > 14999 & esize < .

// collapse by size class, keeping the CDF location of the top of the size bin
collapse (sum) firm estb empl payr rcpt (max) emplcdf*, by(sect naics2 size)


// REVENUE-BASED SIZE CRITERION //
// carve out from large the portion of activity at firms with empl>15,000
// that belongs in medium based on the receipts criterion

reshape wide firm estb empl payr rcpt emplcdf*, i(sect naics2) j(size)

// merge in the emplsize associated with $5b receipts
// sectors *without* firms > $5b will not have entries in the using data
merge 1:1 sect using ../../data/processed/esize_rcpt5b.dta, nogen keepusing(emplcdf*)


foreach aa in firm estb empl payr rcpt {
   // fraction of large activity that should be in medium (pct of total activity)
   gen frac_med_`aa'2 = max(0, emplcdf_`aa'_rcpt5b - emplcdf_`aa'2)
   // multiply by total to get in activity units
   gen `aa'_adj2 = (`aa'0 * frac_med_`aa'2)

   // add activity to medium
   replace `aa'2 = `aa'2 + `aa'_adj2

   // compute large by subtraction so details add total
   replace `aa'3 = `aa'0 - `aa'1 - `aa'2

   drop `aa'_adj2 frac_med_`aa'2
}

// clean up
drop emplcdf*

// reshape to long
reshape long firm estb empl payr rcpt, i(sect naics2) j(size)


/*
// check that the totals match
preserve
drop if size == 0
collapse (sum) firm estb empl payr rcpt, by(sect)

foreach aa in firm estb empl payr rcpt {
   rename `aa' `aa'2
}
merge 1:1 sect using `check', nogen
foreach aa in firm estb empl payr rcpt {
   gen `aa'dif = `aa' - `aa'2
}
format *dif %16.0fc
noi sum *dif, sep(0) for
restore

// they do!
*/


// merge in the shares by LFO3 //

// this joinby drops the total (size=0) class
joinby sect size using `lfo'
order lfo3, after(naics2)
sort sect lfo3 size


// apply size-specific LFO shares
foreach aa in firm estb empl payr rcpt {
   replace `aa' = `aa' * `aa'_shr
   drop `aa'_shr
}


// round final totals to nearest integer
foreach aa in firm estb empl payr rcpt {
   rename `aa' `aa'0
   gen `aa' = round(`aa'0)

   // measure has to be at least 1 if there is any activity
   replace `aa' = 1 if `aa' == 0 & `aa'0 > 0
   drop `aa'0
}

format firm estb empl payr rcpt %16.0fc

/*
// check that the totals match
preserve
collapse (sum) firm estb empl payr rcpt, by(sect)

foreach aa in firm estb empl payr rcpt {
   rename `aa' `aa'2
}
merge 1:1 sect using `check', nogen
foreach aa in firm estb empl payr rcpt {
   gen `aa'dif = `aa' - `aa'2
}
format *dif %16.0fc
noi sum *dif, sep(0) for
restore
*/

// label new size variable
label def l_size 1 "Small", replace
label def l_size 2 "Medium", add
label def l_size 3 "Large", add
label val size l_size


// label data
label var naics2 "Industry code"
label var size "Size class (S, M, L)"
label var firm "Number of firms"
label var estb "Number of establishments"
label var empl "Employment (with noise)"
label var payr "Annual payroll ($1,000, with noise)"
label var rcpt "Annual receipts ($1,000, with noise)"

sort sect lfo3 size

compress
save ../../data/processed/activity_susb_lfo_2019.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  end activity_susb_lfo_2019.do"

if "$S_CONSOLE" == "console" & "$batch" != "1" exit, STATA clear
