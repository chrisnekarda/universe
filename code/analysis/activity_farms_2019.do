// activity_farms_2019.do
//
// Scales up 2017 farms activity to 2019
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin activity_farms_2019.do"

tempfile gdp susb_shr

// GDP FACTORS BY SECTOR //
use ../../data/interim/va_by_industry.dta, clear
tsset sect qdate, q
tscollap gdp, to(y) gen(year)
keep if year == 2017 | year == 2019
reshape wide gdp, i(sect) j(year)
gen gdp_ratio = gdp2019/gdp2017
keep sect gdp_ratio
save `gdp'


// SUSB rbin shares for farming
use ../../data/processed/susb_rcptsize_2017_rcpt5b.dta, clear

drop rsize naics2

keep if sect == 2

// share of activity above $1m in [$1M, $5B)
foreach xx in firm estb empl payr rcpt {
   egen `xx'_shr = pc(`xx') if rbin >= 3 & rbin <= 13, prop by(sect)
}

keep sect rbin *_shr
compress
save `susb_shr'



// FARMS //
use ../../data/interim/activity_farms_2017.dta, clear

gen byte rbin = 1 if fsize <= 9  // 1 - 499,999
replace rbin = 2 if fsize == 10  // 500,000 - 999,999
replace rbin = 13 if fsize == 11  // 1,000,000 to 4,999,999,999 (assume no farms with >$5B in receipts)

collapse (sum) farms empl_hired payr_hired rcpt, by(rbin)

// put these in NAICS 11
gen byte sect = 2

rename farms firm

// scale farm count to 2019 using USDA survey data
// https://quickstats.nass.usda.gov/#3E516C82-5B06-3DCC-AF6D-38CD035F3027
replace firm = firm * (2.0234/2.0420)

// assume 1 estb per farm
gen estb = firm

rename empl_hired empl
rename payr_hired payr

// scale empl to 2019 using USDA survey data
// https://quickstats.nass.usda.gov/#3E516C82-5B06-3DCC-AF6D-38CD035F3027
replace empl = empl * (629/673)

// scale payr to 2019 using USDA survey data (empl * wage)
// https://quickstats.nass.usda.gov/#3E516C82-5B06-3DCC-AF6D-38CD035F3027
replace payr = payr * (629/673) * (14.71/13.23)

// rcpt and payr are in thousands in SUSB
replace rcpt = rcpt/1000
replace payr = payr/1000


// scale receipts by ratio of 2019 to 2017 gross output from NIPA
merge m:1 sect using `gdp', keep(1 3) nogen
replace rcpt = round(rcpt * gdp_ratio)

keep sect rbin firm estb empl payr rcpt

// merge in SUSB shares
merge 1:1 sect rbin using `susb_shr', nogen

// share out the activity in bins 3-13 based on SUSB shares
foreach xx in firm empl payr rcpt {
   egen foo = max(`xx') if rbin == 13
   egen bar = max(foo)
   replace `xx' = bar * `xx'_shr if rbin >= 3 & rbin <= 13
   drop foo bar
}

drop if firm == .
drop *shr

// estabs are firms for farms, so don't share out from naics 11
replace estb = firm

// round to nearest integer
foreach xx in firm estb empl payr rcpt {
   replace `xx' = round(`xx')
}

// merge in naics code
merge m:1 sect using ../../data/interim/xwalk_sect_naics.dta, keepusing(naics2) keep(1 3) nogen

order sect naics2 rbin firm estb empl payr rcpt
sort sect rbin

format firm estb empl payr rcpt %16.0fc

// label variables
label var naics2 "Industry code"
label var firm "Number of firms"
label var estb "Number of establishments"
label var empl "Employment (with noise)"
label var payr "Annual payroll ($1,000, with noise)"
label var rcpt "Estimated receipts ($1,000, with noise)"


compress
save ../../data/processed/activity_farms_2019.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  end activity_farms_2019.do"

if "$S_CONSOLE" == "console" & "$batch" != "1" exit, STATA clear
