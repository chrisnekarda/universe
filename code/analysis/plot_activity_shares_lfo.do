use ../../data/processed/activity_shares_lfo.dta, clear

merge m:1 sect using ../../data/interim/xwalk_sect_naics.dta, nogen keep(3) keepusing(naics2)
order naics2

foreach xx in firm estb empl payr {
   replace `xx'_shr = 100 * `xx'_shr
}

#delim ;
graph bar (asis) firm_shr
if entrsize == 1,
over(naics2, label(angle(90)))
over(lfo3)
ylab(0(20)100) ytitle("")
plotr(m(2.5 2.5 0 0))
legend(off)
title("{bf:Share of Firms, by NAICS and LFO}")
subtitle("Percent", pos(11))
;
#delim cr
graph export ../../output/firm_lfo.pdf, replace

#delim ;
graph bar (asis) estb_shr
if entrsize == 1,
over(naics2, label(angle(90)))
over(lfo3)
ylab(0(20)100) ytitle("")
plotr(m(2.5 2.5 0 0))
legend(off)
title("{bf:Share of Establishments, by NAICS and LFO}")
subtitle("Percent", pos(11))
;
#delim cr
if "$S_CONSOLE" == "console" graph export ../../output/estb_lfo.pdf, replace

#delim ;
graph bar (asis) empl_shr
if entrsize == 1,
over(naics2, label(angle(90)))
over(lfo3)
ylab(0(20)100) ytitle("")
plotr(m(2.5 2.5 0 0))
legend(off)
title("{bf:Share of Employment, by NAICS and LFO}")
subtitle("Percent", pos(11))
;
#delim cr
if "$S_CONSOLE" == "console" graph export ../../output/empl_lfo.pdf, replace

#delim ;
graph bar (asis) payr_shr
if entrsize == 1,
over(naics2, label(angle(90)))
over(lfo3)
ylab(0(20)100) ytitle("")
plotr(m(2.5 2.5 0 0))
legend(off)
title("{bf:Share of Annual Payroll, by NAICS and LFO}")
subtitle("Percent", pos(11))
;
#delim cr
if "$S_CONSOLE" == "console" graph export ../../output/payr_lfo.pdf, replace


if "$S_CONSOLE" == "console" {
   cd ../../output
   !gs -q -dSAFER -dBATCH -dNOPAUSE -sOutputFile=activity_share_lfo.pdf -sDEVICE=pdfwrite -f firm_lfo.pdf estb_lfo.pdf empl_lfo.pdf payr_lfo.pdf
   !rm -f firm_lfo.pdf estb_lfo.pdf empl_lfo.pdf payr_lfo.pdf >&/dev/null
   cd ../code/analysis
}

if "$S_CONSOLE" == "console" & "$batch" != "1" exit, STATA clear
