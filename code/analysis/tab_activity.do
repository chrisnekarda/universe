// tab_activity.do
//
// Collapse to table values
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin tab_activity.do"

tempfile govt byclass sum1 sum2 sum6 sum10 sum20 sum21 sum22 sum23

// define row shade colors
local PPP "ddebf7"
local MST "fce4d6"
local CCF "e2efda"
local MLF "fff2cc"


// load government activity from activity_gov.xlsx //
import excel using ../../data/raw/activity_govt.xlsx, sheet("activity") firstrow clear
drop if row == .

rename class cls
encode cls, gen(class)
replace class = class + 90
drop cls

gen byte sect = 21

save `govt'


// load private-sector activity measures //
use ../../data/processed/activity_universe_2019.dta, replace

drop if class == 7
drop naics2

// correct for duplicated firms at the sector level
rename org_type lfo3
merge 1:1 sect lfo3 size using ../../data/processed/firm_dup.dta, nogen keepusing(firm_dup)

// replace the missing with zeros
replace firm_dup = 0 if firm_dup == .

// subtract duplicated firms from count
replace firm = firm - firm_dup

rename lfo3 org_type


gen row = .
order row

// assign detailed class to table row
replace row =  3 if class == 1
replace row =  4 if class == 2
replace row =  5 if class == 3
replace row =  7 if class == 4
replace row =  8 if class == 5
replace row =  9 if class == 6
replace row = 11 if class == 8
replace row = 12 if class == 9
replace row = 13 if class == 10

// merge in government block
merge m:1 row sect using `govt', nogen


gen row_color = ""
replace row_color = "`PPP'" if row == 3 | row == 7 | row == 11 | row == 13
replace row_color = "`MST'" if row == 4 | row == 8 | row == 12
replace row_color = "`CCF'" if row == 5 | row == 9
replace row_color = "`MLF'" if row == 18 | row == 19

save `byclass'


// private
preserve
collapse (sum) firm estb empl payr rcpt if class != 7 & class <= 10, by(sect)
gen row = 1
save `sum1'
restore

// private, for-profit
preserve
collapse (sum) firm estb empl payr rcpt if class >= 1 & class <= 3, by(sect)
gen row = 2
save `sum2'
restore

// private, nonprofit
preserve
collapse (sum) firm estb empl payr rcpt if class >= 4 & class <= 6, by(sect)
gen row = 6
save `sum6'
restore

// private, farm
preserve
collapse (sum) firm estb empl payr rcpt if class >= 8 & class <= 9, by(sect)
gen row = 10
save `sum10'
restore


// Muni LF, covers state and local governments
preserve
collapse (sum) firm estb empl payr rcpt if row == 18 | row == 19, by(sect)
gen row = 23
gen row_color = "`MLF'"
save `sum23'
restore


// program totals, with PPP adjustments //

use ../../data/processed/activity_universe_2019.dta, replace

foreach xx in firm estb empl payr rcpt {
   rename `xx' `xx'_lfo
}

// merge in NAICS 72 size adjustments for PPP
merge 1:1 sect org_type size using ../../data/processed/activity_naics72_ppp.dta, nogen

foreach xx in firm estb empl payr rcpt {
   egen `xx' = rowtotal(`xx'*)
}


// correct for duplicated firms at the sector level
rename org_type lfo3
merge 1:1 sect lfo3 size using ../../data/processed/firm_dup.dta, nogen keepusing(firm_dup)

// replace the missing with zeros
replace firm_dup = 0 if firm_dup == .

// subtract duplicated firms from count
replace firm = firm - firm_dup

rename lfo3 org_type


// PPP, covers small for-profit, small nonprofit, small farm, and nonemployers
preserve
collapse (sum) firm estb empl payr rcpt if class == 1 | class == 4 | class == 8 | class == 10, by(sect)
gen row = 20
gen row_color = "`PPP'"
save `sum20'
restore

// Main Street, covers medium for-profit, medium nonprofits, and large farms
preserve
collapse (sum) firm estb empl payr rcpt if class == 2 | class == 5 | class == 9, by(sect)
gen row = 21
gen row_color = "`MST'"
save `sum21'
restore

// CCFs, covers large for-profit, large nonprofits
preserve
collapse (sum) firm estb empl payr rcpt if class == 3 | class == 6, by(sect)
gen row = 22
gen row_color = "`CCF'"
save `sum22'
restore



use `byclass', clear

foreach r in 1 2 6 10 20 21 22 23 {
   merge m:1 row sect using `sum`r'', nogen
}

drop size org_type

sort row sect


// rescale employment, payroll, and receipts
replace empl = empl/1e6  // millions
replace payr = payr/1e6  // billions
replace rcpt = rcpt/1e6  // billions

format firm payr rcpt %10.0fc
format empl %10.1fc

save ../../output/tab_activity.dta, replace


// collapse sector-level detail
collapse (sum) firm estb empl payr rcpt (first) row_color, by(row)

// there aren't firms or receipts for the subdivisions of federal govt
replace firm = . if row == 16 | row == 17
replace rcpt = . if row == 16 | row == 17

// screen report
noi list row_color row firm empl payr rcpt if row != ., clean noobs ab(20)

// tex labels for rows
label def l_row  1 "Private"
label def l_row  2 "\pad{1}For-profit", add
label def l_row  3 "\pad{2}Small", add
label def l_row  4 "\pad{2}Medium", add
label def l_row  5 "\pad{2}Large", add
label def l_row  6 "\pad{1}Nonprofit", add
label def l_row  7 "\pad{2}Small", add
label def l_row  8 "\pad{2}Medium", add
label def l_row  9 "\pad{2}Large", add
label def l_row 10 "\pad{1}Farms", add
label def l_row 11 "\pad{2}Small", add
label def l_row 12 "\pad{2}Medium", add
label def l_row 13 "\pad{1}Nonemployers", add
label def l_row 14 "Government", add
label def l_row 15 "\pad{1}Federal", add
label def l_row 16 "\pad{2}Civilian", add
label def l_row 17 "\pad{2}Armed forces", add
label def l_row 18 "\pad{1}State", add
label def l_row 19 "\pad{1}Local", add
label def l_row 20 "PPP", add
label def l_row 21 "Main Street", add
label def l_row 22 "CCFs", add
label def l_row 23 "Muni LF", add

label val row l_row


// create string variables
foreach xx in firm empl payr rcpt {
   tostring `xx', gen(s_`xx') force usedisplayformat
   replace s_`xx' = "" if `xx' == .
}

// calculate longest description
local maxlen 0
forvalues r = 1/23 {
   local desc: label l_row `r'
   local maxlen = max(`maxlen', length("`desc'"))
}


cap file close tabfile
file open tabfile using ../../output/tab_activity.tex, write text replace

// table opening
file write tabfile "\begin{tabularx}{\textwidth}{" _n
file write tabfile "   r" _n
file write tabfile "   X" _n
file write tabfile "   S[table-format=8.0]" _n
file write tabfile "   S[table-format=3.1]" _n
file write tabfile "   S[table-format=4.0]" _n
file write tabfile "   S[table-format=5.0]" _n
file write tabfile "   }" _n
file write tabfile "\toprule" _n
file write tabfile "\multicolumn{2}{l}{\textit{Class or program}} &" _n
file write tabfile "\multicolumn{1}{b{5.5em}}{\centering\textit{Firms or entities}} &" _n
file write tabfile "\multicolumn{1}{b{5.5em}}{\centering\textit{Employment (millions)}} &" _n
file write tabfile "\multicolumn{1}{b{5.5em}}{\centering\textit{Annual payroll (billions)}} &" _n
file write tabfile "\multicolumn{1}{b{5.5em}}{\centering\textit{Annual receipts (billions)}} \\" _n
file write tabfile "\midrule" _n

// items by class
file write tabfile "\multicolumn{2}{l}{\textit{By class*}}\\" _n
forvalues r = 1/19 {
   if row_color[`r'] != "" {
      file write tabfile "\rowcolor[HTML]{" (row_color[`r']) "}" _n
   }
   file write tabfile %2.0f (row[`r']) ". & "
   local desc: label l_row `r'
   file write tabfile "`desc'" _col(`=`maxlen' + 8') "& "
   file write tabfile (s_firm[`r']) " & "
   file write tabfile (s_empl[`r']) " & "
   file write tabfile (s_payr[`r']) " & "
   file write tabfile (s_rcpt[`r']) "\\" _n
}

// items by program
file write tabfile "\\[.5ex]" _n
file write tabfile "\multicolumn{2}{l}{\textit{By program*}}\\" _n
forvalues r = 20/23 {
   if row_color[`r'] != "" {
      file write tabfile "\rowcolor[HTML]{" (row_color[`r']) "}" _n
   }
   file write tabfile %2.0f (row[`r']) ". & "
   local desc: label l_row `r'
   file write tabfile "`desc'" _col(`=`maxlen' + 8') "& "
   file write tabfile (s_firm[`r']) " & "
   file write tabfile (s_empl[`r']) " & "
   file write tabfile (s_payr[`r']) " & "
   file write tabfile (s_rcpt[`r']) "\\" _n
}

// table closing
file write tabfile "\bottomrule" _n
file write tabfile "\end{tabularx}" _n

file close tabfile


noi disp as text "$S_DATE $S_TIME  end tab_activity.do"

if "$S_CONSOLE" == "console" & "$batch" != "1" exit, STATA clear
