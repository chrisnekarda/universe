// activity_naics72_ppp.do
//
// Estimate activity in NAICS 72 eligible for PPP
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin activity_nacis72_ppp.do"

tempfile susb_large susb cbp lfo

// SUSB large share
use ../../data/processed/susb_rcptsize_2017_rcpt5b.dta, clear
keep if sect == 19 & rbin == 14

foreach xx in firm estb empl payr rcpt {
   rename `xx' `xx'_susb_large
}

drop rbin rsize

save `susb_large'


// SUSB data on establishments, employment, and payroll of FIRMS with fewer than 500 employees
use ../../data/processed/susb_emplsize_2017.dta, clear

drop *fl*
rename empl_n empl
rename payr_n payr
rename rcpt_n rcpt

keep if sect == 19
keep if entrsize == 1 | entrsize == 8

keep sect entrsize firm estb empl payr rcpt

foreach xx in firm estb empl payr rcpt {
   rename `xx' `xx'_susb
}

reshape wide *_susb, i(sect) j(entrsize)

foreach xx in firm estb empl payr rcpt {
   rename `xx'_susb1 `xx'_susb
   rename `xx'_susb8 `xx'_susb_small
   gen `xx'_susb_notsmall = `xx'_susb - `xx'_susb_small
}

save `susb'


// CBP data on establishments, employment, and payroll of ESTABLISHMENTS with fewer than 500 employees
use ../../data/interim/cbp_empl500.dta, clear
keep if sect == 19
foreach xx in estb empl payr {
   rename `xx' `xx'_cbp_small
}

save `cbp'


// LFO share of med-large naics 72 (excl govt)
use ../../data/processed/activity_universe_2019.dta, replace
keep if sect == 19
keep if org_type == 1 | org_type == 2

foreach xx in firm estb empl payr rcpt {
   egen `xx'_shr = pc(`xx'), prop by(size)
}

keep sect org_type size *shr

compress
save `lfo'


// Estabs with fewer than 500 emp at firms with more than 500 emp (=cbp-susb)
use `susb', clear
merge 1:1 sect using `susb_large', nogen
merge 1:1 sect using `cbp', nogen
order naics2 sect


// calculate share of notsmall activity at large firms
foreach xx in firm estb empl payr rcpt {
   gen `xx'_large_shr = `xx'_susb_large/`xx'_susb_notsmall
}


// need to move `xx'_2move activity from `xx'_notsmall to `xx'_small
foreach xx in estb empl payr {
   gen `xx'_2move = `xx'_cbp_small - `xx'_susb_small
}


gen firm_estb = firm_susb_notsmall/estb_susb_notsmall
gen rcpt_payr = rcpt_susb_notsmall/payr_susb_notsmall

// calculate firm (from estb) and rcpt (from payr)
gen firm_2move = estb_2move * firm_estb
gen rcpt_2move = payr_2move * rcpt_payr

drop firm_estb rcpt_payr

// calculate the activity being moved to small that comes out of
// large and medium firms using large share of SUSB notsmall activity
foreach xx in firm estb empl payr rcpt {
   gen `xx'_2move2_large = `xx'_2move * `xx'_large_shr
   gen `xx'_2move2_med = `xx'_2move - `xx'_2move2_large

   // flip sign so can be added to totals
   replace `xx'_2move2_large = -`xx'_2move2_large
   replace `xx'_2move2_med = -`xx'_2move2_med
}
drop *large_shr

keep sect naics2 *2move*
format firm* estb* empl* payr* rcpt* %16.0fc


// reshape to create size clas
reshape long firm estb empl payr rcpt, i(naics2 sect) j(foo) str

gen byte size = 0
replace size = 1 if foo == "_2move"
replace size = 2 if foo == "_2move2_med"
replace size = 3 if foo == "_2move2_large"

drop foo

gen byte org_type = 1

expand 2, gen(exp)
replace org_type = 2 if exp == 1
drop exp

// now do the LFO split
merge 1:1 sect size org_type using `lfo', nogen


foreach xx in firm estb empl payr rcpt {
   gen `xx'_ppp = round(`xx' * `xx'_shr)

   // compute nonprofit by subtraction to ensure they add to total
   egen foo = max(`xx'_ppp) if org_type == 1, by(size)
   egen bar = max(foo), by(size)
   replace bar = round(bar)
   replace `xx'_ppp = round(`xx') - bar if org_type == 2
   drop foo bar
}

// move one large firm from for-profit to nonprofit so there
// is one firm left in large nonprofit after the PPP adjustment
// for all the activity to be associated with

replace firm_ppp = -48 if size == 3 & org_type == 1
replace firm_ppp = -2 if size == 3 & org_type == 2


keep sect size org_type *_ppp

compress
save ../../data/processed/activity_naics72_ppp.dta, replace

noi disp as text "$S_DATE $S_TIME  end activity_nacis72_ppp.do"

if "$S_CONSOLE" == "console" & "$batch" != "1" exit, STATA clear
