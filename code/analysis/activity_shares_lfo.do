// activity_shares_lfo.do
//
// Calculate shares of 2017 activity by LFO
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin activity_shares_lfo.do"


use ../../data/processed/susb_emplsize_lfo_2017.dta, clear

drop *fl*
rename empl_n empl
rename payr_n payr

// drop the total
drop if lfo == 1


// create new LFO with only 3 categories
gen byte lfo3 = 1
replace lfo3 = 2 if lfo == 6
replace lfo3 = 3 if lfo == 7

label def l_lfo3 1 "For-profit", replace
label def l_lfo3 2 "Nonprofit", add
label def l_lfo3 3 "Government", add

label val lfo3 l_lfo3
label var lfo3 "Legal form of organization"
order lfo3, after(lfo)

noi tab lfo lfo3


// collapse to the 3 LFO classes
collapse (sum) firm estb empl payr, by(sect lfo3 entrsize)


label var firm "Number of firms"
label var estb "Number of establishments"
label var empl "Employment (with noise)"
label var payr "Annual payroll ($1,000, with noise)"


foreach xx in firm estb empl payr {
   egen `xx'_shr = pc(`xx'), prop by(sect entrsize)
}

clonevar rcpt_shr = payr_shr
note rcpt_shr: Copy of payr_shr

label var firm_shr "Share of firms, by LFO"
label var estb_shr "Share of establishments, by LFO"
label var empl_shr "Share of employment, by LFO"
label var payr_shr "Share of annual payroll, by LFO"
label var rcpt_shr "Share of annual receipts, by LFO"


compress
save ../../data/processed/activity_shares_lfo.dta, replace

noi des, f

// report nonprofit shares of employment (for all size classes)
foreach xx in firm estb empl payr {
   replace `xx'_shr = 100 * `xx'_shr
}

noi disp as result _n(2) "Nonprofit share of employment (all firm size classes)" _c
noi table sect if lfo3 == 2 & entrsize == 1, c(mean empl_shr) format(%5.0f) cellwidth(15)


if "$draw_charts" == "1" noi run plot_activity_shares_lfo.do

noi disp as text "$S_DATE $S_TIME  end activity_shares_lfo.do"

if "$S_CONSOLE" == "console" & "$batch" != "1" exit, STATA clear
