// interp_rcpt5b.do
//
// Estimate a $5 B cut-off in 2017 SUSB rcptsize file and distribute activity
// from [2.5 B, ∞) bin to new [2.5 B, 5 B) and [5 B, ∞) bins //
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

// variable for date-time stamp
local d = string(clock("`c(current_date)' `c(current_time)'", "DMY hms"), "%tcCCYYNNDD!THHMMSS")

cap log close
log using ../../log/interp_rcpt5b_`d'.log, text replace

noi disp as text "$S_DATE $S_TIME  begin interp_rcpt5b.do"
est drop _all

tempfile cdf_top pre_interp


// load top revenue of firms by sector (from Compustat)
tempvar maxmax
use ../../data/interim/cdf_top_2017.dta, clear
drop top_empl
gen byte entrsize_rcpt = 24
rename top_rcpt rsize
replace rsize = round(rsize * 1e6)

// create a total category which is the global maximum
egen `maxmax' = max(rsize)
expand 2 in 1
replace sect = 1 in l
replace rsize = `maxmax' in l
drop `maxmax'
sort sect

save `cdf_top'


// Start with 2017 SUSB rcptsize data
use ../../data/processed/susb_rcptsize_2017.dta, clear

drop *fl*
rename empl_n empl
rename payr_n payr
rename rcpt_n rcpt

drop if entrsize_rcpt == 18 | entrsize_rcpt == 19

// merge in 2017 top firm size data from Compustat
merge 1:1 sect entrsize_rcpt using `cdf_top', update nogen

drop if firm == .

gen byte rbin = .
order rbin, after(entrsize_rcpt)

replace rbin = 0 if entrsize_rcpt == 1
replace rbin = 1 if entrsize_rcpt == 2
replace rbin = 1 if entrsize_rcpt == 3
replace rbin = 2 if entrsize_rcpt == 4
replace rbin = 3 if entrsize_rcpt == 5
replace rbin = 4 if entrsize_rcpt == 6
replace rbin = 5 if entrsize_rcpt == 7
replace rbin = 5 if entrsize_rcpt == 8
replace rbin = 6 if entrsize_rcpt == 9
replace rbin = 6 if entrsize_rcpt == 10
replace rbin = 6 if entrsize_rcpt == 11
replace rbin = 7 if entrsize_rcpt == 12
replace rbin = 7 if entrsize_rcpt == 13
replace rbin = 7 if entrsize_rcpt == 14
replace rbin = 7 if entrsize_rcpt == 15
replace rbin = 8 if entrsize_rcpt == 16
replace rbin = 8 if entrsize_rcpt == 17
replace rbin = 9 if entrsize_rcpt == 20
replace rbin = 10 if entrsize_rcpt == 21
replace rbin = 11 if entrsize_rcpt == 22
replace rbin = 12 if entrsize_rcpt == 23
replace rbin = 14 if entrsize_rcpt == 24


collapse (max) rsize (sum) firm estb empl payr rcpt, by(sect rbin)

// compute empirical CDF
foreach xx in firm estb empl payr rcpt {
   bys sect (rbin): gen cum = sum(`xx') if rbin != 0
   egen tot = max(cum), by(sect)
   gen rcptcdf_`xx' = cum / tot
   drop cum tot
}

// get list of sectors
sum sect
local first_sect = r(min)
local last_sect = r(max)

// some sectors do not have a firm with more than $5 B in receipts, which means
// all activity from [2.5 B, ∞) bin should fall in the [2.5 B, 5 B) bin
// for these, simply replace rbin 14 -> rbin 13
forvalues i = `first_sect'/`last_sect' {
   sum rsize if sect == `i'
   if r(max) < 5e9 {
      replace rbin = 13 if sect ==`i' & rbin == 14
   }
}

save `pre_interp'


// estimating the $5 B cut-off is done separately for each sector
local pdflist ""
cap erase ../../data/interim/interp_rcpt5b.dta

forvalues i = `first_sect'/`last_sect' {
   local sect: label l_sect `i'

   use `pre_interp', clear

   sum firm if sect == `i'
   if r(N) > 0 {

      // keep only sector `i'
      keep if sect == `i'

      sum rsize
      if r(max) >= 5e9 {
         noi disp _n "-------------------------------------------------------------------------------"
         noi disp "`sect'"

         set obs `=_N + 1'
         replace sect = sect[1] in l
         replace rsize = 5e9 - 1 in l
         replace rbin = 13 in l
         sort rbin

         gen lrsize = ln(rsize)
         order lrsize, after(rsize)
         label var lrsize "Size (receipts, dollars, log)"

         // create index for plotting the CDF approximation
         gen ilrsize = ceil(100 * lrsize)
         replace ilrsize = 0 if rsize == 0
         order sect ilrsize
         tsset sect ilrsize
         tsfill

         // remove values not in the range for interpolation
         drop if rbin == . & ilrsize < 1934

         replace rsize = exp(ilrsize/100) if rsize == .
         replace lrsize = ln(rsize) if lrsize == .

         gen lrsize2 = lrsize^2
         order lrsize2, after(lrsize)


         // FIRMS ARE SPECIAL //
         noi disp _n "Activity measure: firm"

         // quadratic
         noi reg rcptcdf_firm lrsize lrsize2 if rsize >= 500e6 - 1
         predict rcptcdf_firm_hat_quad if rsize >= 500e6 - 1, xb

         // beta (logit)
         clonevar rcptcdf_firm2 = rcptcdf_firm
         replace rcptcdf_firm2 = rcptcdf_firm - 1e-7 if rcptcdf_firm == 1
         // use quadratic in lrsize
         noi betareg rcptcdf_firm2 lrsize lrsize2 if rsize >= 500e6 - 1, nolog
         predict rcptcdf_firm_hat_beta if rsize >= 500e6 - 1

         // check that first derivative of fitted CDF doesn't change sign
         noi sum D1.rcptcdf_firm_hat_beta
         if r(min) < 0 & r(max) > 0 {
            // it did, use linear formulation
            drop rcptcdf_firm_hat_beta
            noi betareg rcptcdf_firm2 lrsize if rsize >= 500e6 - 1, nolog
            predict rcptcdf_firm_hat_beta if rsize >= 500e6 - 1
         }
         drop rcptcdf_firm2


         // pick which estimate fits best
         gen rcptcdf_firm_hat = .

         // check that CDF from quadratic is strictly smaller than 1 at $5b
         sum rcptcdf_firm_hat_quad if rsize == 5e9 - 1
         local quad_at_rcpt5b = r(mean)

         noi sum D1.rcptcdf_firm_hat_quad
         local quad_rmin = r(min)
         local quad_rmax = r(max)

         // compute RMSE of quadratic and beta
         noi disp _n "RMSE:"
         noi rmse rcptcdf_firm rcptcdf_firm_hat_quad rcptcdf_firm_hat_beta if rsize >= 500e6 - 1

         // use the quadratic if RMSE is lower *and* estimate < 1 *and* slope of
         // quadratic doesn't change sign
         if real(r(rcptcdf_firm_hat_quad)) < real(r(rcptcdf_firm_hat_beta)) & ///
            `quad_at_rcpt5b' < 1 & ///
            !(`quad_rmin' < 0 & `quad_rmax' > 0) {
            replace rcptcdf_firm_hat = rcptcdf_firm_hat_quad
            local quad_wgt = "medthick"
            local beta_wgt = "medthin"
            local quad_col = "$c8%60"
            local beta_col = "$c9%40"
         }
         // otherwise use beta
         else {
            replace rcptcdf_firm_hat = rcptcdf_firm_hat_beta
            local quad_wgt = "medthin"
            local beta_wgt = "medthick"
            local quad_col = "$c8%40"
            local beta_col = "$c9%60"
         }

         gen rcptcdf_firm_rcpt5b = rcptcdf_firm_hat if rsize == 5e9 - 1


         // ALL MEASURES BUT FIRMS USE ONLY QUADRATIC //
         foreach xx in estb empl payr rcpt {
            noi disp _n "Activity measure: `xx'"

            // quadratic
            noi reg rcptcdf_`xx' lrsize lrsize2 if rsize >= 500e6 - 1
            predict rcptcdf_`xx'_hat if rsize >= 500e6 - 1, xb

            gen rcptcdf_`xx'_rcpt5b = rcptcdf_`xx'_hat if rsize == 5e9 - 1
         }

         if "$draw_charts" == "1" {
            #delim ;

            tw
            (scatter rcptcdf_firm lrsize if rbin != ., yaxis(1 2) msym(O) col("$c7") mfc("$c7%75"))
            (line rcptcdf_firm_hat_quad lrsize, yaxis(1 2) lw(medthick) col("$c8%55"))
            (line rcptcdf_firm_hat_beta lrsize, yaxis(1 2) lw(medthick) col("$c9%55"))
            (scatter rcptcdf_firm_rcpt5b lrsize, yaxis(1 2) msym(dh) col("$c10"))
            if sect == 0 & rsize >= 100e6 - 1,
            xlab(18(1)27, labc(white) labsize(9pt))
            xtitle("")
            ylab(0(.2)1, for(%-7.4g) labc(white) labsize(9pt)) ymtick(1, nolab tlen(5))
            ylab(0(.2)1, axis(2) nolab) ymtick(1, axis(2) nolab tlen(5))
            legend(order(1 "Actual" 2 "Fitted - Quadratic" 3 "Fitted - Logistic" 4 "$5 billion cut-off") size(9pt))
            title("{bf:Legend}", size(9pt))
            graphr(m(0 2 0 0))
            name(legd, replace)
            ;

            tw
            (scatter rcptcdf_firm lrsize if rbin != ., yaxis(1 2) msym(O) col("$c7") mfc("$c7%75"))
            (line rcptcdf_firm_hat_quad lrsize, yaxis(1 2) lw(`quad_wgt') col("`quad_col'"))
            (line rcptcdf_firm_hat_beta lrsize, yaxis(1 2) lw(`beta_wgt') col("`beta_col'"))
            (scatter rcptcdf_firm_rcpt5b lrsize, yaxis(1 2) msym(dh) col("$c10"))
            if rsize >= 100e6 - 1,
            xlab(18(1)27, labsize(9pt))
            xtitle("")
            ylab(#7, for(%-7.4g) labsize(9pt)) ymtick(1, nolab tlen(5))
            ylab(#7, axis(2) nolab) ymtick(1, axis(2) nolab tlen(5))
            legend(off)
            title("{bf:Firm count}", size(9pt))
            graphr(m(0 2 0 0))
            name(firm, replace)
            ;

            tw
            (scatter rcptcdf_estb lrsize if rbin != ., yaxis(1 2) msym(O) col("$c7") mfc("$c7%75"))
            (line rcptcdf_estb_hat lrsize, yaxis(1 2) lw(medthick) col("$c8%55"))
            (scatter rcptcdf_estb_rcpt5b lrsize, yaxis(1 2) msym(dh) col("$c10"))
            if rsize >= 100e6 - 1,
            xlab(18(1)27, labsize(9pt))
            xtitle("")
            ylab(#7, for(%-7.4g) labsize(9pt)) ymtick(1, nolab tlen(5))
            ylab(#7, axis(2) nolab) ymtick(1, axis(2) nolab tlen(5))
            legend(off)
            title("{bf:Establishment count}", size(9pt))
            graphr(m(0 2 0 0))
            name(estb, replace)
            ;

            tw
            (scatter rcptcdf_empl lrsize if rbin != ., yaxis(1 2) msym(O) col("$c7") mfc("$c7%75"))
            (line rcptcdf_empl_hat lrsize, yaxis(1 2) lw(medthick) col("$c8%55"))
            (scatter rcptcdf_empl_rcpt5b lrsize, yaxis(1 2) msym(dh) col("$c10"))
            if rsize >= 100e6 - 1,
            xlab(18(1)27, labsize(9pt))
            xtitle("")
            ylab(#7, for(%-7.4g) labsize(9pt)) ymtick(1, nolab tlen(5))
            ylab(#7, axis(2) nolab) ymtick(1, axis(2) nolab tlen(5))
            legend(off)
            title("{bf:Employment}", size(9pt))
            graphr(m(0 2 0 0))
            name(empl, replace)
            ;

            tw
            (scatter rcptcdf_payr lrsize if rbin != ., yaxis(1 2) msym(O) col("$c7") mfc("$c7%75"))
            (line rcptcdf_payr_hat lrsize, yaxis(1 2) lw(medthick) col("$c8%55"))
            (scatter rcptcdf_payr_rcpt5b lrsize, yaxis(1 2) msym(dh) col("$c10"))
            if rsize >= 100e6 - 1,
            xlab(18(1)27, labsize(9pt))
            xtitle("")
            ylab(#7, for(%-7.4g) labsize(9pt)) ymtick(1, nolab tlen(5))
            ylab(#7, axis(2) nolab) ymtick(1, axis(2) nolab tlen(5))
            legend(off)
            title("{bf:Annual payroll}", size(9pt))
            graphr(m(0 2 0 0))
            name(payr, replace)
            ;

            tw
            (scatter rcptcdf_rcpt lrsize if rbin != ., yaxis(1 2) msym(O) col("$c7") mfc("$c7%75"))
            (line rcptcdf_rcpt_hat lrsize, yaxis(1 2) lw(medthick) col("$c8%55"))
            (scatter rcptcdf_rcpt_rcpt5b lrsize, yaxis(1 2) msym(dh) col("$c10"))
            if rsize >= 100e6 - 1,
            xlab(18(1)27, labsize(9pt))
            xtitle("")
            ylab(#7, for(%-7.4g) labsize(9pt)) ymtick(1, nolab tlen(5))
            ylab(#7, axis(2) nolab) ymtick(1, axis(2) nolab tlen(5))
            legend(off)
            title("{bf:Annual receipts}", size(9pt))
            graphr(m(0 2 0 0))
            name(rcpt, replace)
            ;

            graph combine legd firm estb empl payr rcpt,
            title("{bf:Emprical and Fitted CDFs}" "`sect'", size(9pt))
            note("Log firm receipt size", pos(6) size(8pt))
            ysize(3.0in)
            ;
            #delim cr

            graph drop legd firm estb empl payr rcpt

            if "$S_CONSOLE" == "console" {
               graph export ../../output/rcptcdf_`i'.pdf, replace fontdir(../../lib/fonts) fontface("Charter")
               local pdflist = "`pdflist' rcptcdf_`i'.pdf"
            }
         }

         // save interpolation results
         keep sect ilrsize rbin rsize lrsize rcptcdf*

         cap confirm file ../../data/interim/interp_rcpt5b.dta
         if !_rc {
            noi merge 1:1 sect ilrsize using ../../data/interim/interp_rcpt5b.dta, nogen
            sort sect ilrsize
            compress
            save ../../data/interim/interp_rcpt5b.dta, replace
         }
         else {
            compress
            save ../../data/interim/interp_rcpt5b.dta
         }

         noi disp "-------------------------------------------------------------------------------"
      }
   }
}

// put together chart packet if it was created
if "$S_CONSOLE" == "console" & "`pdflist'" != "" {
   cd ../../output
   !gs -q -dSAFER -dBATCH -dNOPAUSE -sOutputFile=interp_rcpt5b.pdf -sDEVICE=pdfwrite -f `pdflist'
   !rm -f `pdflist' >&/dev/null
   cd ../code/analysis
}


// start from pre-interp activity and CDF measures
use `pre_interp', clear

// merge in estimated $5 B location in CDF, where applicable
merge 1:m sect rbin using ../../data/interim/interp_rcpt5b.dta, nogen keepusing(rsize rcptcdf*_rcpt5b) update
sort sect rbin

// drop the super-fine bins used for plotting the CDF estimates
drop if rbin == .

// fill in the $5 B location in the CDF using the best estimate
foreach xx in firm estb empl payr rcpt {
   replace rcptcdf_`xx' = rcptcdf_`xx'_rcpt5b if rbin == 13 & rcptcdf_`xx' == .
   drop rcptcdf_`xx'_rcpt5b
}

// distribute activity estimates from [2.5 B, ∞) bin to new [2.5 B, 5 B) and
// [5 B, ∞) bins
foreach xx in firm estb empl payr rcpt {
   // this picks up the "total" bin
   egen tot = max(`xx'), by(sect)

   // translate CDF back into activity measure
   gen cum  = round(rcptcdf_`xx' * tot)

   // contribution of each rbin to the total
   // these match original calculation for rbin \in [2, 12]
   bys sect (rbin): gen marg = cum[_n] - cum[_n-1] if rbin != 0

   // replace the activty measure with the new values in the top 2 bins
   // it changes nothing for sectors where there is no activity above $5 B
   replace `xx' = marg if rbin >= 13

   drop tot cum marg
}

/*
// check that the new measures sum to the total
foreach xx in firm estb empl payr rcpt {
   bys sect: gen cum = sum(`xx') if rbin != 0
   egen chk = max(cum), by(sect)
   egen tot = max(`xx'), by(sect)
   gen `xx'_dif = tot - chk
   drop cum chk tot
}
noi sum *_dif
drop *_dif

// they do!
*/


keep sect rbin rsize firm estb empl payr rcpt

merge m:1 sect using ../../data/interim/xwalk_sect_naics.dta, keepusing(naics2) keep(1 3) nogen
order sect naics2

format firm estb empl payr rcpt %16.0fc

// label data
label var naics2 "Industry code"
label var rsize "Firm size (receipts)"
label var firm "Number of firms"
label var estb "Number of establishments"
label var empl "Employment (with noise)"
label var payr "Annual payroll ($1,000, with noise)"
label var rcpt "Estimated receipts ($1,000, with noise)"

compress
save ../../data/processed/susb_rcptsize_2017_rcpt5b.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  end interp_rcpt5b.do"
cap log close

if "$S_CONSOLE" == "console" & "$batch" != "1" exit, STATA clear
