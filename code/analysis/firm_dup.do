// firm_dup.do
//
// Calculate number of duplicated firms at sector level, needed to get
// aggregate firm count right
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin firm_dup.do"

tempfile nolfo

use ../../data/processed/activity_susb_lfo_2019.dta, clear

// sum over LFO
collapse (sum) firm, by(size sect)

// calculate sum of firms over all sectors
egen foo = sum(firm) if sect != 1, by(size)
egen firm_sum = max(foo), by(size)
drop foo

// ratio of firms to firm total in aggregate
gen foo = firm_sum / firm if sect == 1
egen firm_ratio = max(foo), by(size)
drop foo

format firm firm_sum %16.0fc
format firm_ratio %16.3f

noi disp _n "firm count adjustment ratios" _c
noi list size firm firm_sum firm_ratio if sect == 1, clean noobs ab(15)

save `nolfo'


// start with SUSB firm counts by sector, size, and LFO
use ../../data/processed/activity_susb_lfo_2019.dta, clear
keep sect lfo3 size firm
merge m:1 size sect using `nolfo', keepusing(firm_ratio) nogen


// apply the adjustment
gen firm_adj = firm / firm_ratio

// calculate number of duplicated firms
gen firm_dup = firm - round(firm_adj)

format firm_adj firm_dup %16.0fc

keep sect lfo3 size firm_ratio firm_adj firm_dup
drop if sect == 1
sort sect lfo3 size

label var firm_ratio "Ratio of sector-level firm count to total firm count"
label var firm_adj "Firm count adjusted for duplicate firms"
label var firm_dup "Firms duplicated in multiple sectors"

compress
save ../../data/processed/firm_dup.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  end firm_dup.do"

if "$S_CONSOLE" == "console" & "$batch" != "1" exit, STATA clear
