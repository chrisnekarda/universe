// activity_susb_ebin_2019.do
//
// Calculate measures of activity from SUSB data by ebin
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin activity_susb_ebin_2019.do"

tempfile gdp


// GDP FACTORS BY SECTOR //
use ../../data/interim/va_by_industry.dta, clear
tsset sect qdate, q
tscollap gdp, to(y) gen(year)
keep if year == 2017 | year == 2019
reshape wide gdp, i(sect) j(year)
gen gdp_ratio = gdp2019/gdp2017
keep sect gdp_ratio
save `gdp'



// 2017 SUSB //
use ../../data/processed/susb_emplsize_2017_empl15k.dta, clear

foreach xx in firm estb empl payr rcpt {
   rename `xx' `xx'2017
}

// merge in value added by sector
merge m:1 sect using `gdp', keep(1 3) nogen

// merge in QCEW data
merge m:1 sect using ../../data/processed/qcew_ratios.dta, keep(1 3) keepusing(*ratio) nogen

// firm growth ratio using QCEW estb (sector) and BED firm (national)
// https://www.bls.gov/web/cewbd/table_g.txt
gen bed_ratio = 5273 / 5189 if sect == 1
gen foo = (bed_ratio - 1)/(estb_ratio - 1) if sect == 1
// ratio of BED firm browth rate to QCEW estab growth rate
egen bed_qcew_ratio = max(foo)
drop foo

// create new sector-level firm ratio
gen firm_ratio = (estb_ratio - 1) * bed_qcew_ratio + 1


// scale up using ratios
// note: does not enforce adding up by ebin
gen firm = round(firm2017 * firm_ratio)  // QCEW/BED
gen estb = round(estb2017 * estb_ratio)  // QCEW
gen empl = round(empl2017 * empl_ratio)  // QCEW
gen payr = round(payr2017 * payr_ratio)  // QCEW
gen rcpt = round(rcpt2017 * gdp_ratio )  // NIPA

format esize firm estb empl payr rcpt %16.0fc
drop *ratio *2017



// label variables
label var naics2 "Industry code"
label var esize "Firm size (employment)"
label var firm "Number of firms"
label var estb "Number of establishments"
label var empl "Employment (with noise)"
label var payr "Annual payroll ($1,000, with noise)"
label var rcpt "Estimated receipts ($1,000, with noise)"

run ../build/label_ebin.do

compress
save ../../data/processed/activity_susb_ebin_2019.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  end activity_susb_ebin_2019.do"

if "$S_CONSOLE" == "console" & "$batch" != "1" exit, STATA clear
