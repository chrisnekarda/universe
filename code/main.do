// main.do
//
// Main script for creating data and running analysis
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin main.do"

clear all
est drop _all
set more off

// project-wide Stata settings
noi run _config.do

// run programs as part of main script? (0 = no, 1 = yes)
global batch = 1

// draw charts? (0 = no, 1 = yes)
// note PDFs will only be saved if Stata is run from the console
global draw_charts = 1


// build the data
cd build
noi run build_data.do
cd ..

// do the analysis
cd analysis
noi run analysis.do
cd ..


noi disp as text "$S_DATE $S_TIME  end main.do"

if "$S_CONSOLE" == "console" exit, STATA clear
