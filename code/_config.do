// _config.do
//
// Project-wide Stata settings
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

version 16.0
set type double
set varabbrev off
/* set matsize 2000 */

// remove any personal directories from the adopath //
while _rc == 0 {
   cap adopath -4
}


// identify the root directory of the project //

// get current working directory
local cwd = c(pwd)
local len = length("`cwd'")

noi disp "`cwd'"

// if run from ./code
if substr("`cwd'", -4, .) == "code" {
   global root_path = substr("`cwd'", 1, `=`len' - 5')
}
// if not, find the root path of the project by stripping subdirs
foreach xx in analysis build {
   local len1 = length("`xx'")
   if substr("`cwd'", -`len1', .) == "`xx'" {
      global root_path = substr("`cwd'", 1, `=`len' - `len1' - 6')
   }
}
if "$root_path" == "" {
   noi disp as error "_config.do must be run from a directory in ./code"
   error 99
}


// add paths to code and external dependencies //
adopath + "$root_path/code"
adopath + "$root_path/lib"

// update MATA libraries index
mata: mata mlib index

// graphics scheme for figures
discard
set scheme texpdf

// define custom colors (MATLAB Parula scheme) //

// (gray)
global c6  = " 77  77  77"
// (blue)
global c7  = "  0 114 189"
// (orange)
global c8  = "217  83  25"
// (yellow)
global c9  = "237 177  32"
// (purple)
global c10 = "126  47 142"
// (green)
global c11 = "119 172  48"
// (light blue)
global c12 = " 77 190 238"
// (red)
global c13 = "162  20  47"
// (dark green)
global c14 = " 78 112  31"
// forecast shading
global c15 = "224 255 255"
// recession shading
global c16 = "217 217 217"

// set global flag that project configuration has been run
global proj_config = 1
