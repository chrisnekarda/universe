// import_nonemp.do
//
// Import nonemployer data
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin import_nonemp.do"

import delimited ../../data/raw/nonemp17us.txt, clear

compress
save ../../data/interim/nonemp.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  begin import_nonemp.do"
