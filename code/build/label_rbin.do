cap label drop l_rbin
cap label val rbin .

label def l_rbin  0 "Total", replace
label def l_rbin  1 "<499,999", add
label def l_rbin  2 "500,000-999,999", add
label def l_rbin  3 "1,000,000-2,499,999", add
label def l_rbin  4 "2,500,000-4,999,999", add
label def l_rbin  5 "5,000,000-9,999,999", add
label def l_rbin  6 "10,000,000-24,999,999", add
label def l_rbin  7 "25,000,000-49,999,999", add
label def l_rbin  8 "50,000,000-99,999,999", add
label def l_rbin  9 "100,000,000-249,999,999", add
label def l_rbin 10 "250,000,000-499,999,999", add
label def l_rbin 11 "500,000,000-999,999,999", add
label def l_rbin 12 "1,000,000,000-2,499,999,999", add
label def l_rbin 13 "2,499,999,999-4,999,999,999", add
label def l_rbin 14 "5,000,000,000+", add

label val rbin l_rbin
