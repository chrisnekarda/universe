// import_xwalks.do
//
// Import various crosswalk spreadsheets
//
// See instructions in project README. Global environment settings for
// data construction are in build_data.do.
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin import_xwalks.do"

import excel "../../data/raw/xwalk_sect_naics.xlsx", firstrow clear
run label_sect.do

compress
save ../../data/interim/xwalk_sect_naics.dta, replace


noi disp as text "$S_DATE $S_TIME  end import_xwalks.do"

if "$S_CONSOLE" == "console" & "$batch" != "1" exit, STATA clear
