cap label drop l_ebin
cap label val ebin .

label def l_ebin 1  "Total", replace
label def l_ebin 2  "1 to 4", add
label def l_ebin 3  "5 to 9", add
label def l_ebin 4  "10 to 19", add
label def l_ebin 6  "20 to 99", add
label def l_ebin 7  "100 to 499", add
label def l_ebin 10 "500 to 749", add
label def l_ebin 11 "750 to 999", add
label def l_ebin 12 "1,000 to 1,499", add
label def l_ebin 13 "1,500 to 1,999", add
label def l_ebin 14 "2,000 to 2,499", add
label def l_ebin 15 "2,500 to 4,999", add
label def l_ebin 16 "5,000 to 9,999", add
label def l_ebin 17 "10,000 to 14,999", add
label def l_ebin 18 "15,000 to 19,999", add
label def l_ebin 19 "20,000+", add

label val ebin l_ebin
