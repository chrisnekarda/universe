// import_qcew.do
//
// Import 2017 and 2019 QCEW establishments, employment, and payroll
//
// See instructions in project README.
//  https://www.bls.gov/cew/downloadable-data-files.htm
//  https://data.bls.gov/cew/doc/layouts/csv_quarterly_layout.htm
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin import_qcew.do"

// load data for each year
foreach yyyy in 2017 2019 {

   // raw data include only US national (area_fips == "US000"), all estab sizes (size_code = 0)
   insheet using ../../data/raw/`yyyy'_qtrly_singlefile.csv, clear

   // drop the total categories
   drop if own_code == 0 | own_code == 8 | own_code == 9

   // keep only sector level
   rename industry_code naics
   merge m:1 naics using ../../data/interim/xwalk_sect_naics.dta, nogen keep(3) keepusing(sect)

   // use estb and empl count only from Q1 (month 3); sum wages over all 4 quarters
   replace qtrly_estabs = 0 if qtr != 1
   replace month3_emplvl = 0 if qtr != 1

   // sum of all ownership codes
   collapse (sum) qtrly_estabs month3_emplvl total_qtrly_wages, by(year sect)

   compress
   save ../../data/interim/qcew_`yyyy'.dta, replace
}


use ../../data/interim/qcew_2017.dta, clear
merge 1:1 sect year using ../../data/interim/qcew_2019.dta, nogen


// drop the "NEC" sector
drop if sect == 22

rename qtrly_estabs estb
rename month3_emplvl empl
rename total_qtrly_wages payr

// SUSB reports payroll in thousands
replace payr = payr / 1000

format estb empl payr %16.0fc


// create the total category as sum of retained sectors
foreach xx in estb empl payr {
   egen `xx'tot = sum(`xx'), by(year)
}

expand 2 if sect == 2, gen(exp)
replace sect = 1 if exp == 1
sort sect
foreach xx in estb empl payr {
   replace `xx' = `xx'tot if sect == 1
}
drop exp *tot


// convert to wide to construct ratios
reshape wide estb empl payr, i(sect) j(year)

gen estb_ratio = estb2019/estb2017
gen empl_ratio = empl2019/empl2017
gen payr_ratio = payr2019/payr2017


compress
save ../../data/processed/qcew_ratios.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  end import_qcew.do"
