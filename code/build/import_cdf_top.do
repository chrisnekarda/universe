// import_cdf_top.do
//
// Add values for top revenue and employment bins using data from Compustat
//
// See instructions in project README. Global environment settings for
// data construction are in build_data.do.
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin import_cdf_top.do"

clear all

use ../../data/raw/top_firms_2017.dta, clear

drop naics2
rename naics2num naics2

merge m:1 naics2 using ../../data/interim/xwalk_sect_naics.dta, keepusing(sect) keep(3) nogen
order sect

gen top_rcpt = round(top_sale * .79)
gen top_empl = round(top_emp * .75)


collapse (max) top_rcpt (max) top_empl, by(sect)
format top_rcpt top_empl %16.0fc

// create NAICS 55 (not in Compustat) from NAICS 54
// (receipts CDF in 54 is closer to 55 at the top of the distribution than 56)
expand 2 if sect == 13, gen(exp)
replace sect = 14 if sect == 13 & exp == 1
drop exp

sort sect

save ../../data/interim/cdf_top_2017.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  end import_cdf_top.do"

if "$S_CONSOLE" == "console" & "$batch" != "1" exit, STATA clear
