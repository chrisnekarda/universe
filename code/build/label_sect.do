cap label drop l_sect
cap label val sect .

label def l_sect 1  "Total", replace
label def l_sect 2  "Agriculture, forestry, fishing and hunting", add
label def l_sect 3  "Mining, quarrying, and oil and gas extraction", add
label def l_sect 4  "Utilities", add
label def l_sect 5  "Construction", add
label def l_sect 6  "Manufacturing", add
label def l_sect 7  "Wholesale trade", add
label def l_sect 8  "Retail trade", add
label def l_sect 9  "Transportation and warehousing", add
label def l_sect 10 "Information", add
label def l_sect 11 "Finance and insurance", add
label def l_sect 12 "Real estate and rental and leasing", add
label def l_sect 13 "Professional, scientific, and technical services", add
label def l_sect 14 "Management of companies and enterprises", add
label def l_sect 15 "Administrative and waste services", add
label def l_sect 16 "Educational services", add
label def l_sect 17 "Health care and social assistance", add
label def l_sect 18 "Arts, entertainment, and recreation", add
label def l_sect 19 "Accommodation and food services", add
label def l_sect 20 "Other services", add
label def l_sect 21 "Public administration", add
label def l_sect 22 "Industries not classified", add

label val sect l_sect
