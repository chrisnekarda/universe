// download_data.do
//
// Download external data
//
// See instructions in project README. Global environment settings for data
// construction are in build_data.do.
//
// not downloaded by this program:
//   2017-agcensus-chapter1-table2-US.csv  -- https://www.nass.usda.gov/Quick_Stats/CDQT/chapter/1/table/2/state/US
//   2017-agcensus-chapter1-table4-US.csv  -- https://www.nass.usda.gov/Quick_Stats/CDQT/chapter/1/table/4/state/US
//   2017-agcensus-chapter1-table72-US.csv  -- https://www.nass.usda.gov/Quick_Stats/CDQT/chapter/1/table/72/state/US
//   "Data by NAICS and Employment Size.xls"  -- special tabulation from Census Bureau
//   "Data by NAICS and Receipts Size.xls"  -- special tabulation from Census Bureau
//   activity_govt.xlsx  -- Tabulations from Census of Governments and other sources
//   cbp_susb.xlsx  -- County Business Patterns
//   tab_alt_data.xlsx  -- Tabulations from alternative data sources
//   top_firms_2017.dta  -- COMPUSTAT
//   xwalk_sect_naics.xlsx  -- crosswalk by authors
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin download_data.do"

set fredkey 0e62cf7b92100ce83c3197b2ce5bb150  // API key for universe project

// --------------------- //
// QUARTERLY MACRO DATA //
// -------------------- //
// from FRED
clear
noi import fred ///
VAPI ///
VAAER VAAF VAAFH VAAWMS VAC VAES VAFI VAHCSA VAI ///
VAM VAMA VAMCE VAOSEG VAPST VAR VARL VAT VAU VAW, ///
vintage(2020-07-06) clear
gen qdate = qofd(daten)
tsset qdate, q
note qdate: Quarter, 1960:Q1 = 1
order qdate
drop daten datestr
keep if tin(, 2019q4)
save ../../data/raw/va_by_industry.dta, replace


if "$S_OS" != "Windows" {
   cd ../../data/raw

   // 2017 SUSB data
   !wget -N "https://www2.census.gov/programs-surveys/susb/tables/2017/us_6digitnaics_2017.xlsx"

   !wget -N "https://www2.census.gov/programs-surveys/susb/tables/2017/us_state_naics_detailedsizes_2017.txt"

   !wget -N "https://www2.census.gov/programs-surveys/susb/tables/2017/us_naicssector_large_emplsize_2017.xlsx"

   !wget -N "https://www2.census.gov/programs-surveys/susb/tables/2017/us_naicssector_large_rcptsize_2017.xlsx"

   !wget -N "https://www2.census.gov/programs-surveys/susb/tables/2017/us_naicssector_lfo_2017.xlsx"

   // 2017 QCEW single file
   !wget https://data.bls.gov/cew/data/files/2017/csv/2017_qtrly_singlefile.zip
   !unzip -uo 2017_qtrly_singlefile.zip
   // this flat file is massive; keep only records with area_code="US000" and size_code="0"
   !head -n 1 2017.q1-q4.singlefile.csv > 2017_qtrly_singlefile.csv
   !awk -F, '$1~/\"US000\"/ && $5~/\"0\"/ {print}' 2017.q1-q4.singlefile.csv >> 2017_qtrly_singlefile.csv
   !rm 2017.q1-q4.singlefile.csv 2017_qtrly_singlefile.zip

   // 2019 QCEW single file
   !wget https://data.bls.gov/cew/data/files/2019/csv/2019_qtrly_singlefile.zip
   !unzip -uo 2019_qtrly_singlefile.zip
   // this flat file is massive; keep only records with area_code="US000" and size_code="0"
   !head -n 1 2019_qtrly_singlefile > 2019_qtrly_singlefile.csv
   !awk -F, '$1~/\"US000\"/ && $5~/\"0\"/ {print}' 2019_qtrly_singlefile >> 2019_qtrly_singlefile.csv
   !rm 2019_qtrly_singlefile 2019_qtrly_singlefile.zip

   // BED
   !wget -N https://www.bls.gov/web/cewbd/table_g.txt

   // railroad data
   !wget -N "https://www.bts.gov/sites/bts.dot.gov/files/table_rail_profile_111919.xlsx"
   !wget -N "https://www.rrb.gov/sites/default/files/2020-09/TotalEmployment2018.xls"
   !wget -N "https://www.rrb.gov/sites/default/files/2020-05/selectdt.pdf"
   !wget -N "https://www.stb.gov/econdata.nsf/159fd6322aaef606852582d7006be69c/6a65929b365d51c2852583df0051a815/%24FILE/March%202019%20Employment%20Compilation%202000%20Index.xls"
   !wget -N "https://www.stb.gov/econdata.nsf/03630aa15ffe00ef852582d7006e8936/1eaaab98b3972fcb8525850700536c45/%24FILE/EARN-COMP-2019-Q4.xls"

   // nonemployer data
   !wget -N "https://www2.census.gov/programs-surveys/nonemployer-statistics/datasets/2017/historical-datasets/nonemp17us.zip"
   !unzip -uo nonemp17us.zip

      // DMDC - Armed forces
   !wget -N "https://www.dmdc.osd.mil/appj/dwp/rest/download?fileName=DMDC_Website_Location_Report_1903.xlsx&groupName=milRegionCountry"

   cd ../../code/build
}

noi disp as text "$S_DATE $S_TIME  end download_data.do"

if "$S_CONSOLE" == "console" & "$batch" != "1" exit, STATA clear
