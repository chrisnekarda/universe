// import_farms.do
//
// Import farms data
// Notes
//  - of 3.4 million "producers" (owners), 1.4 million report farming as primary
//    occupation while 1.9 million report "other" (Table 52)
//  - 1.3 million producers report zero days of work on the farm. 1.4 million
//    report 200 days or more. (Table 52)
//  - All activity falls in NAICS 111 and 112 (Table 75)
//  - Total sales is roughly half crops and half livestock
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin import_farms.do"


/* Data from:
https://www.nass.usda.gov/Publications/AgCensus/2017/Full_Report/Volume_1,_Chapter_1_US/

Choose Table 2: Market value of ag products sold including landlord's share, food marketing practices, and value-added products: 2017 and 2012.
Use the "Query Tool"
Export CSV under default setup
*/
tempfile rcpt payr empl


// Receipts data, from table 2 //

import delimited "../../data/raw/2017-agcensus-chapter1-table2-US.csv", encoding(UTF-8) clear

gen value2 = subinstr(value, ",", "", .)
destring value2, gen(item) force

keep if dataitem == "COMMODITY TOTALS - SALES, MEASURED IN $" | dataitem == "COMMODITY TOTALS - OPERATIONS WITH SALES"

gen col = 1 if dataitem == "COMMODITY TOTALS - OPERATIONS WITH SALES"
replace col = 2 if dataitem == "COMMODITY TOTALS - SALES, MEASURED IN $"
keep col domaincategory item
rename domaincategory size_desc
replace size_desc = "ALL" if size_desc == ""

reshape wide item, i(size_desc) j(col)
rename item1 farms
rename item2 rcpt


// Wide categories needed for other variables
replace size_desc = "FARM SALES: (10,000 TO 24,999 $)" if size_desc == "FARM SALES: (10,000 TO 19,999 $)" | size_desc == "FARM SALES: (20,000 TO 24,999 $)"
replace size_desc = "FARM SALES: (25,000 TO 49,999 $)" if size_desc == "FARM SALES: (25,000 TO 39,999 $)" | size_desc == "FARM SALES: (40,000 TO 49,999 $)"
drop if size_desc == "FARM SALES: (1,000,000 TO 2,499,999 $)" | size_desc == "FARM SALES: (2,500,000 TO 4,999,999 $)" | size_desc == "FARM SALES: (5,000,000 OR MORE $)"
collapse (sum) farms rcpt, by(size_desc)

label variable farms "Number of farms"
label variable rcpt "Receipts $ excl govt payments"

sort size_desc
save `rcpt', replace



// Payroll data, from table 72 //

import delimited "../../data/raw/2017-agcensus-chapter1-table72-US.csv", encoding(UTF-8) clear
gen value2 = subinstr(value, ",", "", .)
destring value2, gen(item) force

keep if dataitem == "LABOR, HIRED - EXPENSE, MEASURED IN $" | dataitem == "LABOR, CONTRACT - EXPENSE, MEASURED IN $"
gen col = 1 if dataitem == "LABOR, HIRED - EXPENSE, MEASURED IN $"
replace col = 2 if dataitem == "LABOR, CONTRACT - EXPENSE, MEASURED IN $"
keep col domaincategory item
rename domaincategory size_desc

reshape wide item, i(size_desc) j(col)
rename item1 payr_hired
rename item2 payr_contract

label variable payr_hired "Hired worker costs $"
label variable payr_contract "Contract worker costs $"

replace size_desc = "ALL" if size_desc == ""

save `payr'


// Import employment data, from table 72 //

import delimited "../../data/raw/2017-agcensus-chapter1-table72-US.csv", encoding(UTF-8) clear
gen value2=subinstr(value, ",", "", .)
destring value2, gen(item) force

keep if dataitem == "LABOR, HIRED - NUMBER OF WORKERS" | dataitem == "LABOR, UNPAID - NUMBER OF WORKERS"
gen col = 1 if dataitem == "LABOR, HIRED - NUMBER OF WORKERS"
replace col = 2 if dataitem == "LABOR, UNPAID - NUMBER OF WORKERS"
keep col domaincategory item
rename domaincategory size_desc

reshape wide item, i(size_desc) j(col)
rename item1 empl_hired
rename item2 empl_unpaid

label variable empl_hired "Hired workers"
label variable empl_unpaid "Unpaid workers"

save `empl'


use `rcpt', clear
merge 1:1 size_desc using `payr', nogen
merge 1:1 size_desc using `empl', nogen


order size_desc farms empl_hired empl_unpaid payr_hired payr_contract rcpt

replace size_desc = subinstr(size_desc, "FARM SALES: (", "", .)
replace size_desc = subinstr(size_desc, " $)", "", .)

gen fsize = 1 if size_desc == "LESS THAN 1,000"
replace fsize = 2 if size_desc == "1,000 TO 2,499"
replace fsize = 3 if size_desc == "2,500 TO 4,999"
replace fsize = 4 if size_desc == "5,000 TO 9,999"
replace fsize = 5 if size_desc == "10,000 TO 24,999"
replace fsize = 6 if size_desc == "25,000 TO 49,999"
replace fsize = 7 if size_desc == "50,000 TO 99,999"
replace fsize = 8 if size_desc == "100,000 TO 249,999"
replace fsize = 9 if size_desc == "250,000 TO 499,999"
replace fsize = 10 if size_desc == "500,000 TO 999,999"
replace fsize = 11 if size_desc == "1,000,000 OR MORE"

drop if fsize == .

order fsize
sort fsize

lab def l_fsize  1 "LESS THAN 1,000", replace
lab def l_fsize  2 "1,000 TO 2,499", add
lab def l_fsize  3 "2,500 TO 4,999", add
lab def l_fsize  4 "5,000 TO 9,999", add
lab def l_fsize  5 "10,000 TO 24,999", add
lab def l_fsize  6 "25,000 TO 49,999", add
lab def l_fsize  7 "50,000 TO 99,999", add
lab def l_fsize  8 "100,000 TO 249,999", add
lab def l_fsize  9 "250,000 TO 499,999", add
lab def l_fsize 10 "500,000 TO 999,999", add
lab def l_fsize 11 "1,000,000 OR MORE", add

label val fsize l_fsize
drop size_desc

compress
save ../../data/interim/activity_farms_2017.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  begin import_farms.do"
