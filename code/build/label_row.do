label def l_row 1 "Wage and salary", replace
label def l_row 2 "Wage and salary, Nonfarm", add
label def l_row 3 "Wage and salary, Nonfarm, Private", add
label def l_row 4 "Wage and salary, Nonfarm, Government", add
label def l_row 5 "Wage and salary, Farm", add
label def l_row 6 "Sole proprietor", add
