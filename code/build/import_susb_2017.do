// import_susb_2017.do
//
// Import SUSB data from spreadsheets
//
// See instructions in project README. Global environment settings for
// data construction are in build_data.do.
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin import_susb_2017.do"


// -------------------------------------------------------------------------- //
noi disp as text "$S_DATE $S_TIME    importing us_6digitnaics_2017.xlsx"

import excel using ../../data/raw/us_6digitnaics_2017.xlsx, clear

rename A naics
rename B naicsdscr
rename C entr_size
rename D firm
rename E estb
rename F empl_n
rename G emplfl_r
rename H emplfl_n
rename I payr_n
rename J payrfl_n
rename K rcpt_n
rename L rcptfl_n


// drop observations without a firm count (i.e., junk rows)
tempvar foo
destring firm, gen(`foo') force
keep if `foo' != .
drop `foo'
compress


// create categorical size class
gen entrsize = real(substr(entr_size, 1, 2))
order entrsize, after(naicsdscr)
drop entr_size
run label_entrsize.do


// destring numeric values
destring firm estb empl_n payr_n rcpt_n, replace
format firm estb empl_n payr_n rcpt_n %16.0fc


// label data
label var naics "Industry code"
label var entrsize "Enterprise employment size code"
label var firm "Number of firms"
label var estb "Number of establishments"
label var empl_n "Employment (with noise)"
label var emplfl_n "Employment noise flag"
label var payr_n "Annual payroll ($1,000, with noise)"
label var payrfl_n "Annual payroll noise flag"
label var rcpt_n "Estimated receipts ($1,000, with noise)"
label var rcptfl_n "Estimated receipts noise flag"
label var naicsdscr "NAICS industry description"


compress
save ../../data/interim/us_6digitnaics_2017.dta, replace

noi des, f
// -------------------------------------------------------------------------- //



// -------------------------------------------------------------------------- //
noi disp as text "$S_DATE $S_TIME    importing Data by NAICS and Employment Size.xls"

import excel using "../../data/raw/Data by NAICS and Employment Size.xls", clear

rename A naics
rename B naicsdscr
rename C entr_size
rename D firm
rename E estb
rename F empl_n
rename G emplfl_n
rename H payr_n
rename I payrfl_n
rename J rcpt_n
rename K rcptfl_n


// drop observations without a firm count (i.e., junk rows)
tempvar foo
destring firm, gen(`foo') force
keep if `foo' != .
drop `foo'
compress


// create categorical size class
gen entrsize = real(substr(entr_size, 1, 2))
order entrsize, after(naicsdscr)
drop entr_size


// destring numeric values
destring firm estb empl_n payr_n rcpt_n, replace
format firm estb empl_n payr_n rcpt_n %16.0fc


// label data
label var naics "Industry code"
label var entrsize "Enterprise employment size code"
label var firm "Number of firms"
label var estb "Number of establishments"
label var empl_n "Employment (with noise)"
label var emplfl_n "Employment noise flag"
label var payr_n "Annual payroll ($1,000, with noise)"
label var payrfl_n "Annual payroll noise flag"
label var rcpt_n "Estimated receipts ($1,000, with noise)"
label var rcptfl_n "Estimated receipts noise flag"
label var naicsdscr "NAICS industry description"


compress
save ../../data/interim/us_naicssector_large_emplsize_2017.dta, replace

noi des, f
// -------------------------------------------------------------------------- //



/* // -------------------------------------------------------------------------- //
noi disp as text "$S_DATE $S_TIME    importing us_state_naics_detailedsizes_2017.txt"

insheet using ../../data/raw/us_state_naics_detailedsizes_2017.txt, clear

rename ncsdscr naicsdscr

run label_entrsize.do
drop entrsizedscr

format firm estb empl_n payr_n %16.0fc

// label data
label var state "Geographic area code"
label var naics "Industry code"
label var entrsize "Enterprise employment size code"
label var firm "Number of firms"
label var estb "Number of establishments"
label var empl_n "Employment (with noise)"
label var emplfl_r "Employment range flag"
label var emplfl_n "Employment noise flag"
label var payr_n "Annual payroll ($1,000, with noise)"
label var payrfl_n "Annual payroll noise flag"
/* label var rcpt_n "Estimated receipts ($1,000, with noise)" */
/* label var rcptfl_n "Estimated receipts noise flag" */
label var statedscr "State description"
label var naicsdscr "NAICS industry description"


save ../../data/interim/us_state_naics_detailedsizes_2017.dta, replace

noi des, f
// -------------------------------------------------------------------------- // */



// -------------------------------------------------------------------------- //
noi disp as text "$S_DATE $S_TIME    importing Data by NAICS and Receipts Size.xls"

import excel using "../../data/raw/Data by NAICS and Receipts Size.xls", clear


rename A naics
rename B naicsdscr
rename C entr_size
rename D firm
rename E estb
rename F empl_n
rename G emplfl_n
rename H payr_n
rename I payrfl_n
rename J rcpt_n
rename K rcptfl_n


// drop observations without a firm count (i.e., junk rows)
tempvar foo
destring firm, gen(`foo') force
keep if `foo' != .
drop `foo'


// create categorical size class
gen entrsize_rcpt = real(substr(entr_size, 1, 2))
order entrsize_rcpt, after(naicsdscr)
drop entr_size
run label_entrsize_rcpt.do


// destring numeric values
destring firm estb empl_n payr_n rcpt_n, replace
format firm estb empl_n payr_n rcpt_n %16.0fc


// label data
label var naics "Industry code"
label var entrsize_rcpt "Enterprise employment size code (receipts)"
label var firm "Number of firms"
label var estb "Number of establishments"
label var empl_n "Employment (with noise)"
label var emplfl_n "Employment noise flag"
label var payr_n "Annual payroll ($1,000, with noise)"
label var payrfl_n "Annual payroll noise flag"
label var rcpt_n "Estimated receipts ($1,000, with noise)"
label var rcptfl_n "Estimated receipts noise flag"
label var naicsdscr "NAICS industry description"


compress
save ../../data/interim/us_6digitnaics_r_2017.dta, replace
// -------------------------------------------------------------------------- //


// -------------------------------------------------------------------------- //
noi disp as text "$S_DATE $S_TIME    importing us_naicssector_large_rcptsize_2017.xlsx"

import excel using "../../data/raw/us_naicssector_large_rcptsize_2017.xlsx", clear

rename A naics
rename B naicsdscr
rename C entr_size_rcpt
rename D firm
rename E estb
rename F empl_n
rename G emplfl_n
rename H payr_n
rename I payrfl_n
rename J rcpt_n
rename K rcptfl_n


// drop observations without a firm count (i.e., junk rows)
tempvar foo
destring firm, gen(`foo') force
keep if `foo' != .
drop `foo'
compress


// create categorical size class
gen entrsize_rcpt = real(substr(entr_size_rcpt, 1, 1))
order entrsize_rcpt, after(naicsdscr)
drop entr_size_rcpt

label def l_entrsize_rcpt 1 "Total"
label def l_entrsize_rcpt 2 "<100,000", add
label def l_entrsize_rcpt 3 "100,000-249,999", add
label def l_entrsize_rcpt 4 "250,000-499,999", add
label def l_entrsize_rcpt 5 "500,000-999,999", add
label def l_entrsize_rcpt 6 "1,000,000-2,499,999", add
label def l_entrsize_rcpt 7 "2,500,000+", add

label val entrsize_rcpt l_entrsize_rcpt


// destring numeric values
destring firm estb empl_n payr_n rcpt_n, replace
format firm estb empl_n payr_n rcpt_n %16.0fc


// label data
label var naics "Industry code"
label var entrsize_rcpt "Enterprise employment size code"
label var firm "Number of firms"
label var estb "Number of establishments"
label var empl_n "Employment (with noise)"
/* label var emplfl_r "Employment range flag" */
label var emplfl_n "Employment noise flag"
label var payr_n "Annual payroll ($1,000, with noise)"
label var payrfl_n "Annual payroll noise flag"
label var rcpt_n "Estimated receipts ($1,000, with noise)"
label var rcptfl_n "Estimated receipts noise flag"
label var naicsdscr "NAICS industry description"


compress
save ../../data/interim/us_naicssector_large_rcptsize_2017.dta, replace

noi des, f
// -------------------------------------------------------------------------- //

// -------------------------------------------------------------------------- //
noi disp as text "$S_DATE $S_TIME    importing us_naicssector_lfo_2017.xlsx"

import excel using ../../data/raw/us_naicssector_lfo_2017.xlsx, clear

rename A lfo_str
rename B naics
rename C naicsdscr
rename D entr_size
rename E firm
rename F estb
rename G empl_n
rename H emplfl_r
rename I emplfl_n
rename J payr_n
rename K payrfl_n
drop L M

// drop observations without a firm count (i.e., junk rows)
tempvar foo
destring firm, gen(`foo') force
keep if `foo' != .
drop `foo'
compress


// create categorical size class
gen entrsize = real(substr(entr_size, 1, 1))
order entrsize, after(naicsdscr)
drop entr_size

label def l_entrsize_lfo 1 "Total (**)", replace
label def l_entrsize_lfo 2 "0-4", add
label def l_entrsize_lfo 3 "5-9", add
label def l_entrsize_lfo 4 "10-19", add
label def l_entrsize_lfo 5 "<20 (**)", add
label def l_entrsize_lfo 6 "20-99", add
label def l_entrsize_lfo 7 "100-499", add
label def l_entrsize_lfo 8 "<500 (**)", add
label def l_entrsize_lfo 9 "500+", add

label val entrsize l_entrsize_lfo


// create categorical LFO
gen lfo = real(substr(lfo_str, 1, 1))
order lfo, after(lfo_str)
drop lfo_str

label def l_lfo 1 "Total", replace
label def l_lfo 2 "Corporation", add
label def l_lfo 3 "S-Corporation", add
label def l_lfo 4 "Partnership", add
label def l_lfo 5 "Sole Proprietorship", add
label def l_lfo 6 "Nonprofit", add
label def l_lfo 7 "Government", add
label def l_lfo 8 "Other", add

label val lfo l_lfo


// destring numeric values
destring firm estb empl_n payr_n, replace
format firm estb empl_n payr_n %16.0fc


// label data
label var lfo "Legal form of organization"
label var naics "Industry code"
label var entrsize "Enterprise employment size code"
label var firm "Number of firms"
label var estb "Number of establishments"
label var empl_n "Employment (with noise)"
label var emplfl_r "Employment range flag"
label var emplfl_n "Employment noise flag"
label var payr_n "Annual payroll ($1,000, with noise)"
label var payrfl_n "Annual payroll noise flag"
label var naicsdscr "NAICS industry description"


compress
save ../../data/interim/us_naicssector_lfo_2017.dta, replace

noi des, f
// -------------------------------------------------------------------------- //


noi disp as text "$S_DATE $S_TIME  end import_susb_2017.do"

if "$S_CONSOLE" == "console" & "$batch" != "1" exit, STATA clear
