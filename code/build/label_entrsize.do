cap label drop l_entrsize
cap label val entrsize .

label def l_entrsize 1  "Total (**)", replace
label def l_entrsize 2  "1-4", add
label def l_entrsize 3  "5-9", add
label def l_entrsize 4  "10-19", add
label def l_entrsize 5  "<20 (**)", add
label def l_entrsize 6  "20-99", add
label def l_entrsize 7  "100-499", add
label def l_entrsize 8  "<500 (**)", add
label def l_entrsize 9  "500+ (**)", add
label def l_entrsize 10 "500-749", add
label def l_entrsize 11 "750-999", add
label def l_entrsize 12 "1,000-1,499", add
label def l_entrsize 13 "1,500-1,999", add
label def l_entrsize 14 "2,000-2,499", add
label def l_entrsize 15 "2,500-4,999", add
label def l_entrsize 16 "5,000-9,999", add
label def l_entrsize 17 "10,000-19,999", add
label def l_entrsize 18 "20,000+", add
label val entrsize l_entrsize
