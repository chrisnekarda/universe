cap label drop l_entrsize_rcpt
cap label val entrsize_rcpt .

label def l_entrsize_rcpt  1 "Total", replace
label def l_entrsize_rcpt  2 "<100,000", add
label def l_entrsize_rcpt  3 "100,000-499,999", add
label def l_entrsize_rcpt  4 "500,000-999,999", add
label def l_entrsize_rcpt  5 "1,000,000-2,499,999", add
label def l_entrsize_rcpt  6 "2,500,000-4,999,999", add
label def l_entrsize_rcpt  7 "5,000,000-7,499,999", add
label def l_entrsize_rcpt  8 "7,500,000-9,999,999", add
label def l_entrsize_rcpt  9 "10,000,000-14,999,999", add
label def l_entrsize_rcpt 10 "15,000,000-19,999,999", add
label def l_entrsize_rcpt 11 "20,000,000-24,999,999", add
label def l_entrsize_rcpt 12 "25,000,000-29,999,999", add
label def l_entrsize_rcpt 13 "30,000,000-34,999,999", add
label def l_entrsize_rcpt 14 "35,000,000-39,999,999", add
label def l_entrsize_rcpt 15 "40,000,000-49,999,999", add
label def l_entrsize_rcpt 16 "50,000,000-74,999,999", add
label def l_entrsize_rcpt 17 "75,000,000-99,999,999", add
label def l_entrsize_rcpt 18 "100,000,000+", add
label def l_entrsize_rcpt 19 "<100,000,000", add
label def l_entrsize_rcpt 20 "100,000,000-249,999,999", add
label def l_entrsize_rcpt 21 "250,000,000-499,999,999", add
label def l_entrsize_rcpt 22 "500,000,000-999,999,999", add
label def l_entrsize_rcpt 23 "1,000,000,000-2,499,999,999", add
label def l_entrsize_rcpt 24 "2,500,000,000+", add

label val entrsize_rcpt l_entrsize_rcpt
