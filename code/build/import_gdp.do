// import_gdp.do
//
// Minor transformations of raw data from FRED
//
// See instructions in project README. Global environment settings for
// data construction are in build_data.do.
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin import_gdp.do"

use ../../data/raw/va_by_industry.dta, clear

// check for FRED vintage identifier
unab foo: VAOSEG*
local vinstr: subinstr local foo "VAOSEG" ""
// remove vintage identifier, if present
if "`vinstr'" != "" {
   unab foo: VA*
   foreach xx of local foo {
      local bar: subinstr local xx "`vinstr'" ""
      rename `xx' `bar'
   }
}

reshape long VA, i(qdate) j(ind) s

gen byte naics2 = .
replace naics2 = 00 if ind == "PI"  // Private industries
replace naics2 = 11 if ind == "AFH"  // Agriculture, Forestry, Fishing, and Hunting
replace naics2 = 21 if ind == "M"  // Mining, quarrying, and oil and gas extraction
replace naics2 = 22 if ind == "U"  // Utilities
replace naics2 = 23 if ind == "C"  // Construction
replace naics2 = 31 if ind == "MA"  // Manufacturing
replace naics2 = 42 if ind == "W"  // Wholesale trade
replace naics2 = 44 if ind == "R"  // Retail trade
replace naics2 = 48 if ind == "T"  // Transportation and warehousing
replace naics2 = 51 if ind == "I"  // Information
replace naics2 = 52 if ind == "FI"  // Finance and insurance
replace naics2 = 53 if ind == "RL"  // Real estate and rental and leasing
replace naics2 = 54 if ind == "PST"  // Professional, scientific, and technical services
replace naics2 = 55 if ind == "MCE"  // Management of companies and enterprises
replace naics2 = 56 if ind == "AWMS"  // Administrative and waste services
replace naics2 = 61 if ind == "ES"  // Educational services
replace naics2 = 62 if ind == "HCSA"  // Health care and social assistance
replace naics2 = 71 if ind == "AER"  // Arts, entertainment, and recreation
replace naics2 = 72 if ind == "AF"  // Accommodation and food services
replace naics2 = 81 if ind == "OSEG"  // Other services

drop ind
merge m:1 naics2 using ../../data/interim/xwalk_sect_naics.dta, keepusing(sect) keep(1 3) nogen
drop naics2

order sect qdate VA

rename VA gdp
label var gdp "Value added"

sort sect qdate

compress
save ../../data/interim/va_by_industry.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  end import_gdp.do"
