// import_cbp.do
//
// Import CBP data on establishments with fewer than 500 employees
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin import_cbp.do"

import excel "../../data/raw/cbp_susb.xlsx", sheet("cbp") firstrow clear
drop if estabs == .

// merge in sect and keep only 2-digit
merge m:1 naics using ../../data/interim/xwalk_sect_naics.dta, keepusing(sect naics2) keep(3) nogen
order sect naics2

rename estabs estb
rename emp empl
rename payroll payr

keep sect naics2 estb empl payr

format estb empl payr %16.0fc

sort sect

compress
save ../../data/interim/cbp_empl500.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  begin import_cbp.do"
