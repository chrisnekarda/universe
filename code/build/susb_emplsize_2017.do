// susb_emplsize_2017.do
//
// // Merge SUSB data by regular and large employment size files
//
// See instructions in project README. Global environment settings for
// data construction are in build_data.do.
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin susb_emplsize_2017.do"

tempfile large

/*
Size  Enterprise
code  employment size

1	   Total    (**)
2	   1-4
3	   5-9
4	   10-19
5	   <20      (**)
6	   20-99
7	   100-499
8	   <500     (**)
9	   500+     (**)
10	   500-749
11	   750-999
12	   1,000-1,499
13	   1,500-1,999
14	   2,000-2,499
15	   2,500-4,999
16	   5,000-9,999
17	   10,000-19,999
18	   20,000+
*/


tempfile large

use ../../data/interim/us_naicssector_large_emplsize_2017.dta, clear

// drop industries NEC
drop if naics == "99"

// encode sector
merge m:1 naics using ../../data/interim/xwalk_sect_naics.dta, nogen keep(3) keepusing(sect)
drop naics*
order sect

// drop total and <500, which will be same in the small-size file
keep if entrsize >= 3
replace entrsize = entrsize + 7

save `large'


use ../../data/interim/us_6digitnaics_2017.dta, clear

// drop industries NEC
drop if naics == "99"

// encode sector
merge m:1 naics using ../../data/interim/xwalk_sect_naics.dta, nogen keep(3) keepusing(sect)
drop naics*
order sect


merge 1:1 sect entrsize using `large', nogen
sort sect entrsize

run label_entrsize.do

gen int maxsize = .
replace maxsize = 4 if entrsize == 2
replace maxsize = 9 if entrsize == 3
replace maxsize = 19 if entrsize == 4
replace maxsize = 99 if entrsize == 6
replace maxsize = 499 if entrsize == 7
replace maxsize = 749 if entrsize == 10
replace maxsize = 999 if entrsize == 11
replace maxsize = 1499 if entrsize == 12
replace maxsize = 1999 if entrsize == 13
replace maxsize = 2499 if entrsize == 14
replace maxsize = 4999 if entrsize == 15
replace maxsize = 9999 if entrsize == 16
replace maxsize = 19999 if entrsize == 17
order maxsize, after(entrsize)
format maxsize %8.0fc
label var maxsize "Highest employment in size class"


// drop the group subtotals
/* drop if entrsize == 1 | entrsize == 6 | entrsize == 19 | entrsize == 26 */

// drop empty employment range flag (yay!)
drop emplfl_r

// label data
label var sect "Industry code"
label var entrsize "Enterprise employment size code"
label var firm "Number of firms"
label var estb "Number of establishments"
label var empl_n "Employment (with noise)"
/* label var emplfl_r "Employment range flag" */
label var emplfl_n "Employment noise flag"
label var payr_n "Annual payroll ($1,000, with noise)"
label var payrfl_n "Annual payroll noise flag"
label var rcpt_n "Annual receipts ($1,000, with noise)"
label var rcptfl_n "Annual receipts noise flag"


save ../../data/processed/susb_emplsize_2017.dta, replace

noi des, f


noi disp as text "$S_DATE $S_TIME  end susb_emplsize_2017.do"
