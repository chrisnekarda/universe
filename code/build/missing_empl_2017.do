// missing_empl_2017.do
//
// Fill in missing employment in 2017 SUSB LFO tabulation
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin missing_empl_2017.do"

if "$S_CONSOLE" == "console" {
   set linesize 150
}

use ../../data/interim/us_naicssector_lfo_2017.dta, clear

// drop industries NEC
drop if naics=="99"

// encode sector
merge m:1 naics using ../../data/interim/xwalk_sect_naics.dta, nogen keep(3) keepusing(sect)
drop naics*
order sect


// rectangularize data file //

// fill in sector and lfo by entrsize
gen sect_lfo = sect*10 + lfo
tsset sect_lfo entrsize
tsfill, full
tsset, clear

qui replace sect = floor(sect_lfo/10) if sect == .
qui replace lfo = sect_lfo - sect*10 if lfo == .
drop sect_lfo

// fill in entrsize and lfo by sector
gen entrsize_lfo = entrsize*10 + lfo
tsset entrsize_lfo sect
tsfill, full
tsset, clear

qui replace entrsize = floor(entrsize_lfo/10) if entrsize == .
qui replace lfo = entrsize_lfo - entrsize*10 if lfo == .
drop entrsize_lfo

// replace filled-in cells with zeros
foreach xx in firm estb empl_n payr_n {
   qui replace `xx' = 0 if `xx' == .
}

// replace true missings with .
replace empl_n = . if emplfl_r != ""
replace payr_n = . if payrfl_n == "D"

sort sect lfo entrsize


// count how many cells with missing employment per sector/lfo
egen foo = count(firm) if empl_n == ., by(sect lfo)
egen num_nonmiss = max(foo), by(sect lfo)
drop foo
replace num_nonmiss = 0 if num_nonmiss == .

// output starting point to log file
cap log close
log using ../../log/missing_empl_2017_0.log, text replace
noi list sect lfo entrsize firm estb empl_n emplfl_r payr_n if num_nonmiss>0, sepby(sect lfo) noobs
log close

/* only one sector to fill in: utilities (8) */

/*
. noi list sect lfo entrsize firm estb empl_n payr_n emplfl_r if num_nonmiss>0, sepby(sect lfo) noobs

  +---------------------------------------------------------------------------+
  |      sect     lfo     entrsize   firm   estb   empl_n   payr_n   emplfl_r |
  |---------------------------------------------------------------------------|
  | Utilities   Other   Total (**)     16     43      686   30,409            |
  | Utilities   Other          0-4      3      3        .        0          A |
  | Utilities   Other          5-9      3      3       18      436            |
  | Utilities   Other        10-19      0      0        0        0            |
  | Utilities   Other     <20 (**)      7      7       36    2,147            |
  | Utilities   Other        20-99      0      0        0        0            |
  | Utilities   Other      100-499      3     24        .        0          B |
  | Utilities   Other    <500 (**)     11     32       98    4,817            |
  | Utilities   Other         500+      5     11        .        0          F |
  +---------------------------------------------------------------------------+
*/

// these are the overall totals //
sum empl_n if sect == 4 & lfo == 8 & entrsize == 1
local empl_tot = r(mean)
sum payr_n if sect == 4 & lfo == 8 & entrsize == 1
local payr_tot = r(mean)

// these are the <500 totals //
sum empl_n if sect == 4 & lfo == 8 & entrsize == 8
local empl_tot_500 = r(mean)
sum payr_n if sect == 4 & lfo == 8 & entrsize == 8
local payr_tot_500 = r(mean)

// replace 500+ with 'total' minus '<500'
replace empl_n = `empl_tot' - `empl_tot_500' if sect == 4 & lfo == 8 & entrsize == 9
replace payr_n = `payr_tot' - `payr_tot_500' if sect == 4 & lfo == 8 & entrsize == 9


// these are the <20 totals //
sum empl_n if sect == 4 & lfo == 8 & entrsize == 5
local empl_tot = r(mean)
sum payr_n if sect == 4 & lfo == 8 & entrsize == 5
local payr_tot = r(mean)

// these are the >5 totals (just 5-9) //
sum empl_n if sect == 4 & lfo == 8 & entrsize == 3
local empl_tot_5t9 = r(mean)
sum payr_n if sect == 4 & lfo == 8 & entrsize == 3
local payr_tot_5t9 = r(mean)

// replace <5 with '<20' minus '5-9'
replace empl_n = `empl_tot' - `empl_tot_5t9' if sect == 4 & lfo == 8 & entrsize == 2
replace payr_n = `payr_tot' - `payr_tot_5t9' if sect == 4 & lfo == 8 & entrsize == 2


// these are the <500 totals //
sum empl_n if sect == 4 & lfo == 8 & entrsize == 8
local empl_tot = r(mean)
sum payr_n if sect == 4 & lfo == 8 & entrsize == 8
local payr_tot = r(mean)

// these are the <20 totals //
sum empl_n if sect == 4 & lfo == 8 & entrsize == 5
local empl_tot_20 = r(mean)
sum payr_n if sect == 4 & lfo == 8 & entrsize == 5
local payr_tot_20 = r(mean)

// replace <5 with '<20' minus '5-9'
replace empl_n = `empl_tot' - `empl_tot_20' if sect == 4 & lfo == 8 & entrsize == 7
replace payr_n = `payr_tot' - `payr_tot_20' if sect == 4 & lfo == 8 & entrsize == 7

/*
. noi list sect lfo entrsize firm estb empl_n payr_n emplfl_r if num_nonmiss>0, sepby(sect lfo) noobs
  +-------------------------------------------------------------------------+
  |      sect     lfo   entrsize   firm   estb   empl_n   payr_n   emplfl_r |
  |-------------------------------------------------------------------------|
  | Utilities   Other          1     16     43      686   30,409            |
  | Utilities   Other          2      3      3       18    1,711          A |
  | Utilities   Other          3      3      3       18      436            |
  | Utilities   Other          4      0      0        0        0            |
  | Utilities   Other          5      7      7       36    2,147            |
  | Utilities   Other          6      0      0        0        0            |
  | Utilities   Other          7      3     24       62    2,670          B |
  | Utilities   Other          8     11     32       98    4,817            |
  | Utilities   Other          9      5     11      588   25,592          F |
  +-------------------------------------------------------------------------+
*/

// log compare final results
log using ../../log/missing_empl_2017_1.log, text replace
noi disp "final results"
noi list sect lfo entrsize firm estb empl_n emplfl_r payr_n if num_nonmiss>0, sepby(sect lfo) noobs
log close

drop num_nonmiss

compress
save ../../data/processed/susb_emplsize_lfo_2017.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  end missing_empl_2017.do"

if "$S_CONSOLE" == "console" & "$batch" != "1" exit, STATA clear
