// build_data.do
//
// Main script for constructing data files
//
// See instructions in project README.
//
// -------------------------------------------------------------------------- //

// variable for date-time stamp
local d = string(clock("`c(current_date)' `c(current_time)'", "DMY hms"), "%tcCCYYNNDD!THHMMSS")

cap log close
log using ../../log/build_data_`d'.log, text replace

noi disp as text "$S_DATE $S_TIME  begin build_data.do"

// project-wide Stata settings
if "$proj_config" != "1" {
   noi run ../_config.do
}

// run programs as part of main script? (0 = no, 1 = yes)
global batch = 1

// draw charts? (0 = no, 1 = yes)
// note PDFs will only be saved if Stata is run from the console
if "$draw_charts" == "" {
   global draw_charts = 1
}

// data set version
global ver = 1.0


// download data  //
// commented out because data have been committed to ./data/raw
/* noi run download_data.do */

// import crosswalks //
noi run import_xwalks.do

// import top values for CDFs //
noi run import_cdf_top.do

// import GDP by industry //
noi run import_gdp.do

// import the QCEW data and calculate ratios //
noi run import_qcew.do

// import CBP data //
noi run import_cbp.do


// 2017 SUSB data //
noi run import_susb_2017.do
// Merge "regular" and "large" size files
noi run susb_emplsize_2017.do
noi run susb_rcptsize_2017.do
// Fill in missing employment in 2017 LFO tabulation
noi run missing_empl_2017.do
log using ../../log/build_data_`d'.log, text append


// non-SUSB data //
noi run import_farms.do
noi run import_nonemp.do


noi disp as text "$S_DATE $S_TIME  end build_data.do"
cap log close

if "$S_CONSOLE" == "console" & "$batch" != "1" exit, STATA clear
