// susb_rcptsize_2017.do
//
// Merge SUSB data by regular and large receipt size files
//
// See instructions in project README. Global environment settings for
// data construction are in build_data.do.
// -------------------------------------------------------------------------- //

noi disp as text "$S_DATE $S_TIME  begin susb_rcptsize_2017.do"

tempfile large


use ../../data/interim/us_naicssector_large_rcptsize_2017.dta, clear

// drop industries NEC
drop if naics == "99"

// encode sector
merge m:1 naics using ../../data/interim/xwalk_sect_naics.dta, nogen keep(3) keepusing(sect)
drop naics*
order sect

// drop total, which will be same in the small-size file
drop if entrsize_rcpt == 1

// replace categories to number continuously
replace entrsize_rcpt = entrsize_rcpt + 17


save `large'


use ../../data/interim/us_6digitnaics_r_2017.dta, clear

// drop industries NEC
drop if naics == "99"

// encode sector
merge m:1 naics using ../../data/interim/xwalk_sect_naics.dta, nogen keep(3) keepusing(sect)
drop naics*
order sect


// merge in large size classes
merge 1:1 sect entrsize_rcpt using `large', nogen
sort sect entrsize_rcpt

run label_entrsize_rcpt.do


// destring numeric values
destring firm estb empl_n payr_n rcpt_n, replace
format firm estb empl_n payr_n rcpt_n %16.0fc
run label_entrsize_rcpt.do


// revenue size
gen rsize = 0
replace rsize = 99999 if entrsize_rcpt == 2
replace rsize = 499999 if entrsize_rcpt == 3
replace rsize = 999999 if entrsize_rcpt == 4
replace rsize = 2499999 if entrsize_rcpt == 5
replace rsize = 4999999 if entrsize_rcpt == 6
replace rsize = 7499999 if entrsize_rcpt == 7
replace rsize = 9999999 if entrsize_rcpt == 8
replace rsize = 14999999 if entrsize_rcpt == 9
replace rsize = 19999999 if entrsize_rcpt == 10
replace rsize = 24999999 if entrsize_rcpt == 11
replace rsize = 29999999 if entrsize_rcpt == 12
replace rsize = 34999999 if entrsize_rcpt == 13
replace rsize = 39999999 if entrsize_rcpt == 14
replace rsize = 49999999 if entrsize_rcpt == 15
replace rsize = 74999999 if entrsize_rcpt == 16
replace rsize = 99999999 if entrsize_rcpt == 17
replace rsize = 99999999 if entrsize_rcpt == 19
replace rsize = 249999999 if entrsize_rcpt == 20
replace rsize = 499999999 if entrsize_rcpt == 21
replace rsize = 999999999 if entrsize_rcpt == 22
replace rsize = 2499999999 if entrsize_rcpt == 23
replace rsize = . if entrsize_rcpt == 24

format rsize %16.0fc
order rsize, after(entrsize_rcpt)
label var rsize "Size (receipts)"


compress
save ../../data/processed/susb_rcptsize_2017.dta, replace

noi des, f

noi disp as text "$S_DATE $S_TIME  end susb_rcptsize_2017.do"

if "$S_CONSOLE" == "console" & "$batch" != "1" exit, STATA clear
