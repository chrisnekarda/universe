# Across the Universe: Policy Support for Employment and Revenue in the Pandemic Recession

By [Ryan A. Decker](http://rdecker.net), [Robert J. Kurtzman](https://sites.google.com/site/robertkurtzman/), [Byron F. Lutz](https://www.federalreserve.gov/econres/byron-lutz.htm), and [Christopher J. Nekarda](https://chrisnekarda.com)\
*AEA Papers and Proceedings*, Vol. 111 (May 2021)

#### Downloads

* Main paper - [Published](https://www.aeaweb.org/journals/pandp/issues) | [Preprint](https://www.federalreserve.gov/econres/feds/across-the-universe-policy-support-for-employment-revenue-in-pandemic-recession.htm)
* Replication files - [ICPSR](https://doi.org/10.3886/E132421V1) | [GitLab](https://gitlab.com/chrisnekarda/universe/-/archive/aeapp-replication/universe-aeapp-replication.zip)
* [Git repository](https://gitlab.com/chrisnekarda/universe)

## General information

* The [replication archive](https://gitlab.com/chrisnekarda/universe/-/archive/aeapp-replication/universe-aeapp-replication.zip) includes all necessary data files and programs to replicate the paper. The main script is [`code/main.do`](https://gitlab.com/chrisnekarda/universe/-/blob/main/code/main.do); this will process the data and reproduce the paper's results.
* Programs are written for Stata (v16.0) and have been tested under macOS/Linux and Windows. Programs are intended to be executed using `run` (rather than `do`).
* `main.do` must be run from the `code` subdirectory and can be run either interactively or in batch mode. *Graphics output is only saved when run from a console (noninteractive) session.*

### Project directory structure

```
[/path/to/project/directory/]
├── LICENSE
├── README.md        <-- this file
├── data             <-- data files
│   ├── interim      <-- intermediate data produced during processing
│   ├── processed    <-- processed data for analysis
│   └── raw          <-- original source data files
├── lib              <-- external dependencies (e.g., packages added to base Stata)
├── log              <-- log files
├── output           <-- program output (e.g., estimation results, data, etc.)
└── code             <-- source code
    ├── analysis     <-- main paper calculations, tables, and figures
    └── build        <-- data construction
```

## Replication instructions

The steps below will run the replication in **macOS/Linux**. Programs will run in **Windows** only with graphics output disabled (set `draw_charts = 0` on line 23 of [`code/main.do`](https://gitlab.com/chrisnekarda/universe/-/blob/main/code/main.do#L23)).

1. Download the replication archive from [ICPSR](https://doi.org/10.3886/E132421V1) or [GitLab](https://gitlab.com/chrisnekarda/universe/-/releases/aeapp-replication). Extract the archive and change to the main project directory.
1. From a terminal prompt, enter:
   ```shell
   cd code
   stata -q run main.do
   ```

Alternatively, to do this via `git`, clone the [project from GitLab](https://gitlab.com/chrisnekarda/universe) and run the main script `code/main.do`:

```shell
cd [/path/to/working/directory/]
git clone https://gitlab.com/chrisnekarda/universe.git
cd universe
git -c advice.detachedHead=false checkout aeapp-replication
cd code
stata -q run main.do
```

### Updating or changing the source data

The replication archive contains all the data required to construct the data set and run the analysis.
These steps describe how to download or otherwise change the source data.

The program [`code/build/download_data.do`](https://gitlab.com/chrisnekarda/universe/-/blob/main/code/build/download_data.do) downloads all data that are available from public sources.
These files (plus some others that are not directly fetchable) have been saved in `data/raw` and hence this program is commented out in [`build_data.do`](https://gitlab.com/chrisnekarda/universe/-/blob/main/code/build/build_data.do#L37); to download the source data again, either run `download_data.do` on its own (from `code/build`) or uncomment L37 in `build_data.do`.

Some data are fetched from FRED and the command is set to retrieve the vintage we used for our analysis.
To change the vintage of data from FRED, edit `download_data.do` and change `vintage(XXX)` (line 36) to the desired date.
Caution: non-FRED data sources do not respect this vintage date.

Finally, most of the data are fetched using command line tools that are only available by default in macOS/Linux; `download_data.do` will not fetch these files under Windows without modifications.
(Again, all data necessary to replicate the paper are already included in the archive.)
